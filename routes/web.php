<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Auth::routes();
Route::post("/check_login", "Auth\LoginController@checkLogin")->name('check_login');
Route::post("/admin_login","Auth\LoginController@adminLogin")->name('admin_login');

Route::get('/admin/login', 'AdminController@showLoginForm');
Route::get('/register/user','Auth\RegisterController@show')->name('registration');
Route::get('/register/instructor', 'InstructorController@show');
Route::post('/register/instructor', 'InstructorController@store')->name('registerInstructor');
Route::get('/getDistrict', 'InstructorController@getDistrict');
Route::get('/getOrder/{tanggal}/{status}', 'InstructorController@getOrder');
Route::get('/getOrderInstructor/{status}', 'InstructorController@getOrderInstructor')->name('adminOrder');
Route::get('/getOrderInstructor/{status}', 'InstructorController@getOrderInstructor');
Route::get('/getOrderAll', 'InstructorController@getOrderAll');
Route::get('/getAllInstructor', 'AdminController@getAllInstructor')->name('getAllInstructor'); 
Route::get('/help', 'HomeController@showHelp')->name('help');
Route::get('/about', 'HomeController@showAbout')->name('about');
Route::get('/register/show', 'Auth\RegisterController@showForm')->name('showRegister');
Route::post('/register/megatix_user', 'Auth\RegisterController@createUser')->name('register_user');
Route::post('/password/reset', 'Auth\RegisterController@sendEmailResetPasswordMegatix')->name('password_reset');
Route::post('/order/send_mail', 'Auth\RegisterController@order_email')->name('order_email');


Route::middleware('auth')->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home/{type}', 'HomeController@level');
    Route::get('/order_list/{user_id}', 'OrderController@order_list');
    Route::post('/order', 'OrderController@store')->name('order');
    Route::get('/order/pending', 'OrderController@pending')->name('pending');
    Route::post('/order/payment', 'OrderController@payment')->name('payment');
    Route::post('/order/pickdate', 'OrderController@pickdate')->name('pickdate');
    Route::get('/order/pickdate', 'OrderController@pickdate');
    Route::get('/order/summary/{order_id}', 'OrderController@summary')->name('summary');
    Route::get('/order/cancel/{order_id}', 'OrderController@cancel')->name('cancel');
    Route::post('/order/cancel', "OrderController@cancelOrder")->name('cancelOrder');
    Route::get('/order/{type}/{price_list_id}', 'OrderController@order');
    Route::get('/showLogoutForm', 'HomeController@showLogoutForm')->name('showLogoutForm');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::post('/logout_megatix', 'Auth\LoginController@logout_megatix')->name('logout_megatix');
    Route::get('/list_order', 'OrderController@datatables_order_list')->name('list_order');
});

Route::middleware('admin')->group(function(){
    Route::get('/admin', 'AdminController@show');
    Route::get('/admin/order/datatable', 'AdminController@datatableOrderList');
    //Route::get('/getOrderInstructor/{status}', 'InstructorController@getOrderInstructor')->name('adminOrder');
    Route::get('/admin/show', 'AdminController@show')->name('adminOrder');
    Route::get('/admin/detail/{id}', 'AdminController@detail')->name('detailOrder');
    Route::post('/admin/update', 'AdminController@assignInstructor')->name('updateOrder');
    Route::post('/admin/reject', 'AdminController@rejectOrder')->name('rejectOrder');
    Route::get('/admin/user', 'AdminController@user')->name('adminUser');
    Route::get('/admin/instructor', 'AdminController@instructor')->name('adminInstructor');
    Route::post('/admin/instructor/store', 'AdminController@store_instructor')->name('adminInstructorStore');
    Route::post('/admin/instructor/update', 'AdminController@update_instructor')->name('adminInstructorUpdate');
    Route::get('/admin/instructor/delete', 'AdminController@delete_instructor')->name('adminInstructorDelete');
    Route::get('/admin/user/get', 'AdminController@getAllUser')->name('getAllUser');
});

Route::group(['middleware' => 'App\Http\Middleware\InstructorMiddleware'], function(){
    Route::get('/instructor', 'InstructorController@home')->name('instructor');
    Route::get('/instructor/detail/{id}', 'InstructorController@detail')->name('detailOrderInstructor');
    Route::post('/instructor/accept', 'InstructorController@acceptOrder')->name('acceptOrder');
    Route::post('/instructor/decline', 'InstructorController@declineOrder')->name('declineOrder');
});