<?php

namespace App\Http\Controllers;

use App\Model\Order;
use App\Model\Product;
use App\Exception\Hanlder;
use App\User;
use Illuminate\Http\Request;
use App\Mail\OrderConfirmed;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;  

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin')->except('showLoginForm');
    }

    public function show(Request $request){
        // $date = str_replace('/', '-', $request->date);
        // $tgl = date("Y-m-d", strtotime($date));
        $status = $request->status;
        $orders = Order::where('status', $status)
                        ->limit(25)
                        ->latest()
                        ->get();
                        
        return view('admin.index', compact('orders'));
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function registerAdmin()
    {
        return view('admin.register');
    }

    public function getOrderInstructor($status){
        $order = Order::where('status', $status)
                        ->limit(25)
                        ->latest()
                        ->get();
        return \Response::json($order);
    }


    public function detail($id){
        $order = Order::where('id', $id)->first();
        $product = Product::where('id', $order->product_id)->first();
        $date = date("M d, Y", strtotime($order->date));
        return view('admin.detail', compact('order', 'product','date'));
    }

    public function getAllInstructor(){
        $instructor = User::where('type', 'instructor')->get();
        //dd($instructor);
        return response()->json(['data' => $instructor]);
    }

    public function assignInstructor(Request $request){
        // dd($request->order_id);
        // update order with instructor id
        //$instructor = User::where('name', $user)->first();
        try{
            $order = Order::find($request->order_id);
            $order->instructor_id = $request->instructor_id;
            $order->status = 'assigned';
            $user = User::findOrFail($order->user_id);
            Mail::to($user)->send(new OrderConfirmed($order));
            $order->save();
            return response()->json([
                'response' => 200,
                'message' => 'Success Assign Instructor'
            ]);
        } catch(Exception $e){
            return response()->json([
                'response' => 400,
                'message' => $e
            ]);
        }
        // return redirect()->route('detailOrder', ['id' => $order_id ]);
    }

    public function rejectOrder(Request $request){
        // dd($request->order_id);
        // update order with instructor id
        //$instructor = User::where('name', $user)->first();
        $order = Order::find($request->order_id);
        $order->status = 'rejected';
        $order->save();
        return response()->json([
            'response' => 200,
            'message' => 'SUCCESS REJECTED ORDER'
        ]);
        // return redirect()->route('detailOrder', ['id' => $order_id ]);
    }

    public function datatableOrderList(){
        if ($request->ajax()) {
            $data = Order::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('adminOrder.order',compact('order'));
    }

    public function getAllUser(){
        $user = User::where('type', 'user')->get();
        return response()->json(['data' => $user]);
    }

    public function user(){
        return view('admin.user');
    }

    public function instructor(){
        return view('admin.instructor');
    }

    private function validateForm(Request $request){
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'max:12'],
            'profile_image' => ['required', 'image','mimes:jpg,png,jpeg']
        ]);
    }

    private function setAttributes(User $item, Request $request){
        $item->name = $request->name;
        $item->email = $request->email;
        $item->password = Hash::make($request->password);
        $item->type = 'instructor';
        $item->phone = $request->phone;
        $item->user_picture = base64_encode(file_get_contents($request->file('profile_image')->path()));
        //dd($item->user_picture);
    }

    public function store_instructor(Request $request){
        $this->validateForm($request);

        $item = new User();
        $this->setAttributes($item, $request);

        $status = $item->save();

        if($status){
            return back()->with('success', "Success");
        }else{
            return back()->with('error', "error ndes");
        }
    }

    public function update_instructor(Request $request){
        $user = User::where('email', $request->edit_email)->first();
        $user->name = $request->edit_name;
        $user->phone = $request->edit_phone;
        if ($request->hasFile('edit_profile_image')){
            $user->user_picture = base64_encode(file_get_contents($request->file('edit_profile_image')->path()));
        }
        $status = $user->save();
        
        if($status){
            return back()->with('success', "Success");
        }else{
            return back()->with('error', "error ndes");
        }
    }

    public function delete_instructor(Request $request){
        
        $user = User::find($request->id);
        //dd($user);
        
        $status = $user->delete();
    }
    
}
