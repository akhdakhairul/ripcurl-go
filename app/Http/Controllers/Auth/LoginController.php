<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Session\Store;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    | 
    */

    use AuthenticatesUsers;
    //use ResetsPasswords;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function redirectTo(){
    //     $user = Auth::user();
    //     $role = $user->type;
        
    //     if($role == "instructor"){
    //         return '/instructor';
    //     }
    //     else if($role =="admin"){
    //         return '/admin/show';
    //     }
    //     else if($role =="user"){
    //         Auth::logout();
    //         dd()    
    //         return '/admin/login';
    //     }
    //     else{
    //         return '/admin/login';
    //     }
    // }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function adminLogin(Request $request){
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if (Auth::user()->type == 'admin'){
                return redirect()->route('adminOrder');
            } else if (Auth::user()->type == 'instructor'){
                return redirect()->route('instructor');
            } else {
                $this->logout($request);
                throw ValidationException::withMessages([
                    $this->username() => [trans('auth.failed')],
                ]);
            }
        }else{
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
    }

    function checkLogin(Request $request){
        $credentials = $request->only('email', 'password');
        
        $login_megatix = $this->login_megatix($request->email, $request->password)->getContent();
        $obj = json_decode($login_megatix, true);
        
        if ($obj["status"] == 200){
            //berhasil logi nmegatix
            if (Auth::attempt($credentials)) {
                //berhasil login sistem
                if (Auth::user()->type == 'user'){
                    //berhasil login sebagai user
                    session(['auth.currentUser' => $obj['data']]);
                    return redirect()->route('home');
                }else{
                    //gagal login sebagai user
                    $this->logout($request);
                    throw ValidationException::withMessages([
                        $this->username() => [trans('auth.failed')],
                    ]);
                }
            }else{
                //ganti password user yang sudah ada
                if ($this->check_user_db($request->email)){
                    $user = User::where("email", $request->email)->first()->update(['password' => Hash::make($request->password)]);
                    //dd($user);
                    //$user->password = Hash::make($request->password);
                    //$user->save();
                }else{
                    //buat user baru lalu login dengan user
                    User::create([
                        'name' => $request->email,
                        'email' => $request->email,
                        'type' => 'user',
                        'password' => Hash::make($request->password),
                        'user_picture' => 'base64:png, something'
                    ]);
                }

                if(Auth::attempt(["email" => $request->email, "password" => $request->password])){
                    session(['auth.currentUser' => $obj['data']]);
                    return redirect()->route('home');
                }else{
                    throw ValidationException::withMessages([
                        $this->username() => [trans('auth.failed')],
                    ]);
                }
            }
        }else{
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
    }


    public function login_megatix($email, $password)
    {
        try{
            $client = new Client();
            $response = $client->post('https://megatix.com.au/api/v2/accounts/login', [
                'form_params' => [
                    'email' => $email,
                    'password' => $password
                ]
            ]);
            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();
            return response()->json([
                "status" => 200,
                "data" => json_decode($body)
            ]);
        }catch(\GuzzleHttp\Exception\RequestException $e){
            $this->errors = json_decode($e->getResponse()->getBody()->getContents());
            return response()->json([
                "status" => 422,
                "data" => $this->errors
            ]);
        }
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        
        $this->logout_megatix();
        return $this->loggedOut($request) ?: redirect('/');
    }

    public function logout_megatix()
    {
        try{
            $client = new Client();
            $response = $client->post('https://megatix.com.au/api/v2/accounts/logout');
            $body = $response->getBody()->getContents();
            return json_decode($body);
        }catch(\GuzzleHttp\Exception\RequestException $e){
            $this->errors = json_decode($e->getResponse()->getBody()->getContents());
            return response()->json([
                "status" => 422,
                "data" => $this->errors
            ]);
        }
    }

    public function check_user_db($email){
        $isExists = User::where("email", $email)->count();
        if($isExists == 0){
            return false;
        }else {
            return true;
        }
        //dd($isExists);
    }
}
