<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Model\Order;
use App\Mail\OrderConfirmed;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Mail;  
use GuzzleHttp\Client;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     * override redirect so instructor can redirect to their own homepage
     * 
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'type' => 'user',
            'phone' => $data['phone'],
            'user_picture' => '',
            'password' => Hash::make($data['password']),
        ]);
    }

    protected function createUser(Request $request){
        //dd($request->name, $request->email,$request->password, $request->phone);
        $register_megatix = $this->register_megatix($request->name, $request->email,$request->password, $request->phone)->getContent();
        $obj = json_decode($register_megatix, true);

        if($obj["status"] == 200){
            $this->register($request);
            session(['auth.currentUser' => $obj['data']]);
            return redirect()->route('home');
        }else{
            throw ValidationException::withMessages([
                $this->email() => "Email Already Exists, Use Login Instead",
            ]);
        }
    }//asdfjjwoinie@gmail.com

    function email(){
        return "email";
    }

    public function show()
    {
        return view('auth.register');
    }

    public function showForm()
    {
        return view('auth.registration_form');
    }

    public function register_megatix($name, $email, $password, $phone)
    {
        $name_arr = explode(" ", $name);
        $name_length = count($name_arr);
        $i = 1;
        $last_name = "";
        $first_name = "";
        if ($name_length > 1){
            $first_name = $name_arr[0];
            while($i < $name_length){
                $last_name = $last_name." ".$name_arr[$i];
                $i++;
            }
        }else{
            $first_name = $name_arr[0];
            $last_name = $name_arr[0];
        }
        
        try{
            $client = new Client();
            $response = $client->post('https://megatix.com.au/api/v2/accounts/register', [
                'form_params' => [
                    'email' => $email,
                    'password' => $password,
                    'password_confirmation' => $password,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $phone,
                    'terms' => true
                ]
            ]);
            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();
            return response()->json([
                "status" => 200,
                "data" => json_decode($body)
            ]);
        }catch(\GuzzleHttp\Exception\RequestException $e){
            $this->errors = json_decode($e->getResponse()->getBody()->getContents());
            return response()->json([
                "status" => 422,
                "data" => $this->errors
            ]);
        }
    }

    public function sendEmailResetPasswordMegatix(Request $request){
        //dd($request);
        try{
            $client = new Client();
            $response = $client->post('https://megatix.com.au/api/v2/accounts/forgot-password', [
                'form_params' => [
                    'email' => $request->email
                ]
            ]);
            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();
            if (json_decode($body)->message != "success"){
                return back()->with('error', trans(json_decode($body)->message));
            }else{
                return back()->with('success', "Success sending email reset password");
            }
        }catch(\GuzzleHttp\Exception\RequestException $e){

            $this->errors = json_decode($e->getResponse()->getBody()->getContents());
            //dd($this->errors);
            return back()->with('status', trans($this->errors));
        }
    }

    public function order_email(Request $request){
        $order = Order::findOrFail($request->orderId);
        $user = User::findOrFail($order->user_id);
        //return (new OrderConfirmed($order))->render();
        //dd($user);
        Mail::to($user)->send(new OrderConfirmed($order));
    }

    public function sendOrderEmail(Request $request){
        return response()->json([
            "status" => 200,
            "data" => $user
        ]);
    }
}
