<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Model\Product;
use App\Model\Order;
use App\Model\Level;
use App\User;
use App\Mail\OrderConfirmed;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;

class OrderController extends Controller
{
    //
    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function order($type, $price_list_id)
    {
        $level = Level::where('product_id', $price_list_id)->first();
        return view('order.order',['type' => $type, 'price_list_id' => $price_list_id, 'level' => $level->level]);
    }

    public function order_list($user_id){
        if (Auth::user()->id == $user_id){
            $pending_count = Order::where("user_id", $user_id)
                                    ->where("status", "pending")
                                    ->where("date","<", date("Y-m-d"))->count();
            if ($pending_count > 0){
                Order::where("user_id", $user_id)->where("status", "pending")->where("date","<", date("Y-m-d"))->update(['status' => 'expired']);
            }
            $assigned_count = Order::where("user_id", $user_id)
                                ->where("status", "assigned")
                                ->where("date","<", date("Y-m-d"))->count();
            if ($assigned_count > 0){
                Order::where("user_id", $user_id)->where("status", "assigned")->where("date","<", date("Y-m-d"))->update(['status' => 'completed']);
            }
            $order_list = Order::where("user_id", $user_id)->orderBy('updated_at', 'desc')->limit(10)->get();
            return view('orderlist', ['order_list' => $order_list, 'user_id' => $user_id]);
        }else{
            abort(403, 'unauthorized action');
        }
    }

    public function summary($order_id)
    {
        $user_id = Auth::user()->id;
        $order = Order::where("id", $order_id)->first();
        if ($user_id == $order->user_id){
            $date = date("M d, Y", strtotime($order->date));
            $instructor = User::where("id", $order->instructor_id)->first();
            return view('order.summary', 
            [
                'user_id' => $user_id, 
                'order' => $order, 
                'instructor' => $instructor,
                'date' => $date
            ]);
        }else{
            abort(403, 'unauthorized action');
        }
    }

    public function cancel($order_id)
    {
        $user_id = Auth::user()->id;
        $order = Order::where("id", $order_id)->first();
        if ($user_id == $order->user_id){
            $date = date("M d, Y", strtotime($order->date));
            return view('order.cancel', 
            [
                'user_id' => $user_id, 
                'order' => $order, 
                'date' => $date
            ]);
        }else{
            abort(403, 'unauthorized action');
        }
    }

    public function cancelOrder(Request $request){
        // dd($request->order_id);
        // update order with instructor id
        //$instructor = User::where('name', $user)->first();
        $order = Order::find($request->order_id);
        $order->status = 'canceled';
        $order->save();
        return response()->json([
            'response' => 200,
            'message' => 'SUCCESS CANCELED ORDER'
        ]);
        // return redirect()->route('detailOrder', ['id' => $order_id ]);
    }

    public function pending()
    {
        return view('order.pending');
    }

    public function success(){
        return view('order.success');
    }
    
    public function pickdate(Request $request)
    {
        $product = Product::where('id', $request->price_list_id)->first();
        return view('order.date', [
            'product' => $product,
            'type' => $request->type,
            'location_to' => $request->address_address,
            'latitude' => $request->address_latitude,
            'longitude' => $request->address_longitude 
        ]);
    }

    public function store(Request $request)
    {
        /*
           $request->product_id
           $request->surf_location
           $request->user_id
           $request->price
           $request->date
           $request->time

           "product_id" => "13"
            "surf_location" => "-8.6735678,115.26345549999996"
            "user_id" => "123"
            "price" => "1650000.00"
            "date" => "21/05/2019"
            "time" => "8-9"
            "action" => null
            
        */
        
        $item = new Order();
        $this->setAttributes($item, $request);

        $status = $item->save();

        if($status){
            return response()->json([
                'response' => 200,
                'message' => 'Order Success, Continue with payment'
            ]);
        }else{
            return response()->json([
                'response' => 400,
                'message' => 'Bad Request',
                'error' => $status
            ]);
        }

    }
    
    private function splitTime($time){
        return explode('-', $time);
    }

    private function setAttributes(Order $item, Request $request){
        $product = Product::where('id', $request->price_list_id)->first();
         //ambil dari session login
        $item->surf_location = $request->address_address;
        $item->product_id = $product->id;
        $item->price = $product->price;
        $item->status = 'pending';
        $item->user_id = Auth::user()->id;
        $date = str_replace('/', '-', $request->date);
        $tgl = date("Y-m-d", strtotime($date));
        $item->date = $tgl;
        // $email = User::where("email", $request->email)->first();
        // if($email == null){
        //     $this->createUser($request->email, $request->phone);
        //     $getUserId = User::where("email", $request->email)->first();
        //     $item->user_id = $getUserId->id;
        // }else{
        //     $item->user_id = $email->id;
        // }
        // $item->date = $request->date;
        // $item->time_start = $this->splitTime($request->time)[0];
        // $item->time_stop = $this->splitTime($request->time)[1];
        $item->surf_longitude_latitude = $request->address_latitude.','.$request->address_longitude; //$request->surf_longitude_latitude;
        $item->pickup_location = $request->pickup_latitude.','.$request->pickup_longitude;
        
    }

    protected function createUser($email, $phone)
    {
        //dd($data);
        return User::create([
            'name' => $email,
            'email' => $email,
            'type' => 'user',
            'phone' => $phone,
            'password' => Hash::make("12345678"),
            'user_picture' => 'base64, something'
        ]);
    }    

    public function payment(){
        try{
            $client = new Client();
            $response = $client->post('https://megatix.co.id/white-label/surf', [
                'form_params' => [
                    'auth.currentUser' => '{"id":135782,"name":"Dina Rahmawati","email":"dina.rahmawati@gmail.com","phone":"085740949098"}'
                ]
            ]);
            dd($response);
            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();
            return response()->json([
                "status" => 200,
                "data" => json_decode($body)
            ]);
        }catch(\GuzzleHttp\Exception\RequestException $e){
            $this->errors = json_decode($e->getResponse()->getBody()->getContents());
            return response()->json([
                "status" => 422,
                "data" => $this->errors
            ]);
        }
    }
    
    public function datatables_order_list(Request $request){
        //dd($request);
        if ($request->ajax()){
            $data = Order::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editOrder">Edit</a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteOrder">Delete</a>';
 
                         return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('adminOrder.order', compact('orders'));
    }

  
}
