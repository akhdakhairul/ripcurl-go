<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Model\Province;
use App\Model\Regency;
use App\Model\District;
use App\Model\Address;
use App\Http\Requests;
use App\Model\Order;
use App\Model\Product;
use DB;

class InstructorController extends Controller
{
    public function __construct(){
        $this->middleware('instructor')->except('show', 'store');
    }

    public function show(){
        return view('instructor.index');
    }
    
    public function home(){
        $surfs = Order::where('status', 'surfing')->limit(25)->latest()->get();
        $pendings = Order::where('status', 'pending')->limit(25)->latest()->get();
        $completes = Order::where('status', 'completed')->limit(25)->latest()->get();
        return view('instructor.home', compact('surfs', 'pendings', 'completes'));
    }
    
    public function detail($id){

        $order = Order::where('id', $id)->first();
        $product = Product::where('id', $order->product_id)->first();
        return view('instructor.detail', compact('order', 'product'));
    }

    public function index(Request $request){
        return view('instructor.index');
    }

    private function validateForm(Request $request){
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'max:12']
        ]);
    }

    private function setAttributes(User $item, Request $request){
        $item->name = $request->name;
        $item->email = $request->email;
        $item->password = Hash::make($request->password);
        $item->type = 'instructor';
        $item->phone = $request->phone;
        $item->user_picture = "abc";

    }

    private function setAddressAttributes(Address $item, Request $request, $user_id){
        $item->user_id = $user_id;
        $item->district_id = $request->district_id[0];
        $item->postal_code = $request->postal_code;
        $item->address = $request->address;
        // $item->location = $request->location;
    }
    
    public function getOrder($tanggal, $status){
        /* limit, tanggal, status tambah limit dan orderby*/
        $order = Order::where('date', $tanggal)
                        ->where('status', $status)
                        ->limit(25)
                        ->latest()
                        ->get();
        return \Response::json($order);
    }
    
    public function getOrderAll(){
        /* limit, tanggal, status */
        $order = Order::all();
        return \Response::json($order);
    }

    public function getOrderInstructor($status){
        $order = Order::where('status', $status)
                        ->limit(25)
                        ->latest()
                        ->get();
        return \Response::json($order);
    }

    public function acceptOrder(Request $request){
        $order = Order::find($request->id);
        $order->status = "surfing";
        $order->save();
        return response()->json([
            'response' => 200,
            'message' => 'Order Successfully Accept'
        ]);
    }

    public function declineOrder(Request $request){
        //dd($request->id);
        $order = Order::find($request->id);
        $order->status = "rejected";
        $order->save();
        return response()->json([
            'response' => 200,
            'data' => $order,
            'message' => 'You declined the order'
        ]);
    }


    public function store(Request $request){
        $this->validateForm($request);

        $item = new User();
        $this->setAttributes($item, $request);
        
        $status = $item->save();
        if($status){
            Auth::login($item);
            return redirect()->route('registration');
        }else{
            return redirect()->route('registerInstructor');
        }
    }

    public function getDistrict(Request $request){
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $districts = District::search($term)->limit(5)->get();


        $formatted_tags = [];

        foreach ($districts as $district){
            $regency = Regency::find($district->regency_id);
            $province = Province::find($regency->province_id);
            $formatted_tags[] = ['id' => $district->id, 'text' => ucwords($district->district).", ".ucwords($regency->regency).", ".ucwords($province->province)];
        }
        return \Response::json($formatted_tags);
    }

}
