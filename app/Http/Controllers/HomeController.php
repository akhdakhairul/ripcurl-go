<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        return view('home', ['user_id' => $user_id]);
    }

    public function level($type){
        
        
        return view('user.level', [ 'type' => $type]);
    }

    public function showLogoutForm (){
        return view('auth.logout');
    }

    public function showHelp(){
        return view('user.help');
    }

    public function showAbout(){
        return view('user.about');
    }
}
