<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
class InstructorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() == null){
            abort(403, 'unauthorized action');
        }else{
            if ($request->user()->type != 'admin'){
                abort(403, 'unauthorized action');
            }
        }
        return $next($request);
    }
}
