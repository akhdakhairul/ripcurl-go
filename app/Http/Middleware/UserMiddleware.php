<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() && $request->user()->type != 'user'){
            abort(403, 'unauthorized action');
            //return view('unauthorized')->with('role', 'user');
        }
        return $next($request);
    }
}
