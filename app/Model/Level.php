<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    //

    protected $table = 'level';
    protected $fillable = ['product_id','level'];
}
