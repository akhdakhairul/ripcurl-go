<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;


class District extends Model
{
    //
    use Eloquence;

    protected $table = 'm_district';
    
    protected $searchableColumns = ['district'];

    protected $fillable = ['district'];

}
