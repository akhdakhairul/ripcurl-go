<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';
    public $timestamps = true; 
    protected $fillable = ['user_id','surf_location','product_id','date','time_start','time_stop','price', 'surf_longitude_latitude', 'pickup_location'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
