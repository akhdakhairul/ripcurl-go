<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $table = 't_address';

    protected $fillable = ['user_id', 'district_id', 'postal_code', 'address', 'location'];
}
