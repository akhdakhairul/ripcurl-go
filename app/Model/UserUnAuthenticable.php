<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserUnAuthenticable extends Model
{
    //
    protected $table = 'users';

    protected $fillable = ['instructor_id'];
}
