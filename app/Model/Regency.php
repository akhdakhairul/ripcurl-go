<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    //
    protected $table = 'm_regencies';

    public function district()
    {
        return $this->hasMany('App\Model\District');
    }
}
