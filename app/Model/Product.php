<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $table = 'price_list';

    protected $fillable = ['id'];
}
