<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    //
    protected $table = 'm_provinces';

    public function regency()
    {
        return $this->hasMany('App\Model\Regency');
    }
}
