<?php

namespace App\Mail;

use App\Model\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $order;

    public function __construct(Order $order)
    {
        //
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@ripcurlschoolofsurf.com')
                    ->markdown('vendor.notifications.order_email')
                    ->with([
                        'introLines' => ['Thank you for your order, we hope you enjoyed surfing with us.'],
                        'level' => 'success',
                        'actionUrl' => 'https://lacakaja.id:55401/',
                        'outroLines' => 'Login using this account',
                        'username' => 'Guest@ripcurlschoolofsurf.com', 
                        'password' => '123456',
                        'actionText' => 'Lacak'                        
                    ]);
    }
}
