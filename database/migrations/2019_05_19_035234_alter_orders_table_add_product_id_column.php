<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTableAddProductIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orders', function(Blueprint $table){
            $table->unsignedBigInteger('product_id')->after('user_id');
            $table->date('date');
            $table->time('time_start');
            $table->time('time_stop'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orders', function(Blueprint $table){
            $table->dropColumn('product_id');
            $table->dropColumn('date');
            $table->dropColumn('time_start');
            $table->dropColumn('time_stop');
        });
    }
}
