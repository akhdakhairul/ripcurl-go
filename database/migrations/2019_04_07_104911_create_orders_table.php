<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->unsignedBigInteger('instructor_id')->nullabe();
            $table->unsignedBigInteger('user_id');
            $table->string('type'); //adult, child, adult & child
            $table->integer('qty'); // 1/2
            $table->enum('level', ['beginner','little','experience']);
            $table->string('location_at'); //lokasi instructor
            $table->string('location_to')->nullable(); //pantai apa, 
            $table->enum('status',['pending','surfing','completed','rejected','canceled'])->default('pending');
            /*
                pending - pertama kali order langsung default ke pending
                surfing - ketika instructor menerima orderan 
                completed - instructor menekan tombol completed
                rejected - instructor menolak orderan
                canceled - user membatalkan order
            */
            $table->time('elapsed_time')->nullable(); //    
            $table->decimal('price', 10, 2)->default(0);
            $table->decimal('instructor_rating', 5, 2)->nullable();
            $table->timestamps();

            $table->foreign('instructor_id')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
