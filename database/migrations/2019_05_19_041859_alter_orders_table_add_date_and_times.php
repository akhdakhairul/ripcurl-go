<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTableAddDateAndTimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orders', function(Blueprint $table){
            $table->dropColumn('location_at');
            $table->dropColumn('location_to');
        });
        Schema::table('orders', function(Blueprint $table){
            $table->string('instructor_location');
            $table->string('surf_location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orders', function(Blueprint $table){
            $table->dropColumn('instructor_location');
            $table->dropColumn('surf_location');
        });
        Schema::table('orders', function(Blueprint $table){
            $table->string('location_at');
            $table->string('location_to');
        });
    }
}
