<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('m_provinces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('province',255);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('m_regencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('regency',255);
            $table->unsignedInteger('province_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('province_id')->references('id')->on('m_provinces');
        });

        Schema::create('m_district', function (Blueprint $table) {
            $table->increments('id');
            $table->string('district',255);
            $table->unsignedInteger('regency_id');
            $table->timestamps();

            $table->foreign('regency_id')->references('id')->on('m_regencies');
        });

        Schema::create('t_address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('district_id');
            $table->integer('postal_code')->nullable();
            $table->string('address', 500)->nullable();
            $table->string('location')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('district_id')->references('id')->on('m_district');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
        Schema::dropIfExists('t_address');
        Schema::dropIfExists('m_subdistrict');
        Schema::dropIfExists('m_regencies');
        Schema::dropIfExists('m_provinces');
    }
}
