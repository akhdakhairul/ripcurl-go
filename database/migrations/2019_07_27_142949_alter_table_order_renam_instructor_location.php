<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrderRenamInstructorLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('instructor_location');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->string('pickup_location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('pickup_location');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->string('instructor_location');
        });
    }
}
