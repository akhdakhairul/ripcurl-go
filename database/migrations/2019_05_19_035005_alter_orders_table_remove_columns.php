<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTableRemoveColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orders', function(Blueprint $table){
            $table->dropColumn('type');
            $table->dropColumn('level');
            $table->dropColumn('elapsed_time');
            //$table->integer('qty')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orders', function(Blueprint $table){
            $table->string('type');
            $table->enum('level', ['beginner','little','experience']);
            $table->time('elapsed_time')->nullable(); 
            //$table->integer('qty')->nullable();
        });
    }
}
