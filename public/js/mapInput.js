var maps;
function initialize() {

    $('form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });


    const surf_latitude = parseFloat(document.getElementById("address-latitude").value) || -8.67;
    const surf_longitude = parseFloat(document.getElementById("address-longitude").value) || 115.20;

    const map = new google.maps.Map(document.getElementById('address-map'), {
        center: {lat: surf_latitude, lng: surf_longitude},
        zoom: 8,
        disableDefaultUI: true,
        styles: [
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#7fc8ed"
                    },
                    {
                        "saturation": 55
                    },
                    {
                        "lightness": -6
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#7fc8ed"
                    },
                    {
                        "saturation": 55
                    },
                    {
                        "lightness": -6
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#83cead"
                    },
                    {
                        "saturation": 1
                    },
                    {
                        "lightness": -15
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#f3f4f4"
                    },
                    {
                        "saturation": -84
                    },
                    {
                        "lightness": 59
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#ffffff"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffffff"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#bbbbbb"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 26
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffcc00"
                    },
                    {
                        "saturation": 100
                    },
                    {
                        "lightness": -35
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffcc00"
                    },
                    {
                        "saturation": 100
                    },
                    {
                        "lightness": -22
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.school",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#d7e4e4"
                    },
                    {
                        "saturation": -60
                    },
                    {
                        "lightness": 23
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]
    });

    maps = map;

    $("#btnGroup .btn").on("click", function(event){
        //var geocoder = new google.maps.Geocoder();
        var val = $(this).find('input').val();
        var text = $(this).find('input').data('name');
        var level = $("#level").val();
        var latlngStr = val.split(',', 2);
        $("#address-input").val(text);
        $("#address-latitude").val(latlngStr[0]);
        $("#address-longitude").val(latlngStr[1]);
        var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
        maps.setCenter(latlng);
        
        removeMarkers();
        placeMarker(text, maps, level);
        $("#submitSurf").hide();
        //removeMarkers();
        //geocodeLatLng(geocoder, maps, val);  
    })

    
    const autocompletes = [];
    const locationInputs = document.getElementsByClassName("pickup-input");
    for (let i = 0; i < locationInputs.length; i++) {

        const input = locationInputs[i];
        const fieldKey = input.id.replace("-input", "");
        console.log(fieldKey);
        const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';

        const marker = new google.maps.Marker({
            map: map
        });
        maps = map;
        marker.setVisible(isEdit);
        var BALI_BOUNDS = {
            north: -6.98,
            south: -8.50,
            east: 114.41,
            west: 115.85
        };
    
        const autocomplete = new google.maps.places.Autocomplete(input, {
            bounds: BALI_BOUNDS
        });

        autocomplete.key = fieldKey;
        autocomplete.setComponentRestrictions(
            {'country': ['id']});
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
        autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
    }

    for (let i = 0; i < autocompletes.length; i++) {
        const input = autocompletes[i].input;
        const autocomplete = autocompletes[i].autocomplete;
        const map = autocompletes[i].map;
        const marker = autocompletes[i].marker;

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            const lat = place.geometry.location.lat();
            const lng = place.geometry.location.lng();
            setLocationCoordinates(autocomplete.key, lat, lng);

            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                input.value = "";
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        });
    }
}

function removeMarkers(){
    for(i=0; i<markers.length; i++){
        markers[i].setMap(null);
    }
}

function setLocationCoordinates(key, lat, lng) {
    const latitudeField = document.getElementById(key + "-" + "latitude");
    const longitudeField = document.getElementById(key + "-" + "longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    var map = new google.maps.Map(document.getElementById('address-map'), {
        zoom: 10,
        disableDefaultUI: true,
        center: {lat: -8.67, lng: 115.20}
    });
    directionsDisplay.setMap(map);
    var surf_location_latlng = $("#address-latitude").val() + "," + $("#address-longitude").val();
    var pickup_location_latlng = lat + "," + lng;
    calculateAndDisplayRoute(directionsService, directionsDisplay, surf_location_latlng, pickup_location_latlng);
    
    //$("#submitOrder").show();
    $(".date").show();
}

var marker, markers = [];
function placeMarker(location, map, level) {
    var locations;
    console.log(level)
    if (location == "Canggu"){
        if (level == 0){
            locations = [
                ["Old's Man", -8.6593006, 115.1308194],
                ['Berawa Beach', -8.6638801, 115.1359524],   
                ['Echo Beach', -8.655188, 115.1254255]
            ];
        }else if (level == 1 || level == 4){
            locations = [
                ["Old's Man", -8.6593006, 115.1308194]
            ];
        }else if (level == 2){
            locations = [
                ["Old's Man", -8.6593006, 115.1308194],
                ['Berawa Beach', -8.6638801, 115.1359524]
            ];
        }else if(level == 3){
            locations = [
                ['Berawa Beach', -8.6638801, 115.1359524],   
                ['Echo Beach', -8.655188, 115.1254255]
            ];
        }
        maps.setZoom(14);
    }else if(location == "Seminyak"){
        locations = [
            ["Ku De Ta", -8.6930007,115.1584774]
        ];
        maps.setZoom(18);
    }else if(location == "Legian"){
        if(level == 0 || level == 2){
            locations = [
                ["Double Six", -8.696179,115.1607016],
                ["Ripcurl School Of Surf", -8.6990196,115.1623692]
            ];
        }else if(level == 1){
            locations = [
                ["Ripcurl School Of Surf", -8.6990196,115.1623692]
            ];
        }else if(level == 3 || level == 4){
            locations = [
                ["Double Six", -8.696179,115.1607016]
            ];
        }
        maps.setZoom(17);
    }else if(location == "Kuta"){
        if (level == 0 || level == 3){
            locations = [
                ["Padma", -8.7068428,115.1655942],
                ["Wyndham", -8.7116165,115.166839],
                ["Reef", -8.741088,115.1605993],
                ["Airports", -8.7436159,115.1623222]
            ];
        }else if (level == 1 || level == 4){
            locations = [
                ["Wyndham", -8.7116165,115.166839]
            ];
        }else if(level == 2){
            locations = [
                ["Wyndham", -8.7116165,115.166839],
                ["Reef", -8.741088,115.1605993]
            ];
        }        
        maps.setZoom(13.75);
    }else if(location == "Kedungu"){
        locations = [
            ["Kedungu",-8.6088824,115.083265]
        ];
        maps.setZoom(14);
    }else if(location == "Klotok"){
        locations = [
            ["Klotok",-8.5771925,115.4013873]
        ];
        maps.setZoom(14);
    }else if(location == "Serangan"){
        locations = [
            ["Serangan",-8.7334166,115.2279852]
        ];
        maps.setZoom(15);
    }else if(location == "Sanur"){
        locations = [
            ["Sanur",-8.673649999999999,115.26342929999998]
        ];
        maps.setZoom(17);
    }else if(location == "Uluwatu"){
        if(level == 0 || level == 3){
            locations = [
                ["Balangan",-8.7917115,115.1231536],
                ["Dreamland",-8.7992214,115.1177415],
                ["Bingin",-8.805642,115.1130604],
                ["Impossibles",-8.8074581,115.1088268],
                ["Baby Padang",-8.8109885,115.1033904],
                ["Padang Padang",-8.8109885,115.1033904],
                ["Uluwatu",-8.8149189,115.0883183]
            ];
        }else if(level == 1){
            locations = [
                ["Balangan",-8.7917115,115.1231536]
            ];
        }else if(level == 2){
            locations = [
                ["Dreamland",-8.7992214,115.1177415],
                ["Baby Padang",-8.8109885,115.1033904]
            ];
        }        
        maps.setZoom(13);
    }else if(location == "Jimbaran"){
        locations = [
            ["Jimbaran",-8.7765798,115.1664561]
        ];
        maps.setZoom(17);
    }
    var infowindow = new google.maps.InfoWindow();

    for(i=0; i< locations.length; i++){
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            icon: $("#base_url").val() + "/img/maps_marker_resize.png"
        });
        markers.push(marker);
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              $("#address-input").val(locations[i][0]);
              $("#address-latitude").val(locations[i][1]);
              $("#address-longitude").val(locations[i][2]);
              $("#submitSurf").show();
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
    }
    console.log(markers)
}

$(".map-input").on("input", function(){
    var address_input = $("#address-input").val(); 
});

function geocodeLatLng(geocoder, map,  input) {
    var result = "";
    var latlngStr = input.split(',', 2);
    var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {
          map.setCenter(latlng);
          map.setZoom(17);
          marker = new google.maps.Marker({
            position: latlng,
            map: map
          });
          result = results[0].formatted_address;

          $(".pickup-input").val(result);
          console.log(results[0].formatted_address);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
    return result;
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, surf_location, pickup_location) {
    //var selectedMode = document.getElementById('mode').value;
    var surflatlngStr = surf_location.split(',', 2);
    var pickuplatlngStr = pickup_location.split(',', 2);
    var surflatlng = {lat: parseFloat(surflatlngStr[0]), lng: parseFloat(surflatlngStr[1])};
    var pickuplatlng = {lat: parseFloat(pickuplatlngStr[0]), lng: parseFloat(pickuplatlngStr[1])};
    directionsService.route({
      origin: pickuplatlng,  // Haight.
      destination: surflatlng,  // Ocean Beach.
      // Note that Javascript allows us to access the constant
      // using square brackets and a string value as its
      // "property."
      travelMode: google.maps.TravelMode["DRIVING"]
    }, function(response, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
}

document.getElementById("currentLocation").addEventListener("click", function(){
	if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      

      setLocationCoordinates("pickup", position.coords.latitude, position.coords.longitude);
      var val = position.coords.latitude + "," + position.coords.longitude;
      var geocoder = new google.maps.Geocoder();
      geocodeLatLng(geocoder, maps, val);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
})

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}