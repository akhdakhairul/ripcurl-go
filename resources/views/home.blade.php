@extends('layouts.app')
@section('content')
<div id="mySidepanel" class="sidepanel">
    <a href="javascript:void(0)" class="closebtn">&times;</a>
    <a href="{{ url('home') }}">Home</a>
    <a href="{{ url('/order_list/') }}/{{ $user_id }}">Orders</a>
    <a href="{{ url('about') }}">About</a>
    <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a> 
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
<div id="session"></div>
<div class="container-fluid" id="dashboard">
    <input type="hidden" value="{{ $user_id }}">
    <div class="justify-content-center">
        <div class="form-group">
            <div class="row red">
                <button class="openbtn">
                    <i class="fas text-white fa-lg fa-align-justify"></i>   
                </button> 
                <div class="mx-auto my-auto" style="padding-right:50px;">
                    <label class="text-white">GO SURF</label>
                </div>
            </div>
            <!-- <div class="form-group row less-red pt-2">
                <div class="col-4">
                    <a href="{{ url('home') }}">
                        <div class="row justify-content-center">
                            <i class="fas fa-home fa-3x white"></i>    
                        </div>
                        <div class="row justify-content-center">
                            <label>Home</label>
                        </div>

                    </a>
                </div>
                <div class="col-4">
                    <a href="#">
                        <div class="row justify-content-center">
                            <i class="fas fa-question-circle fa-3x white"></i>
                        </div>
                        <div class="row justify-content-center">
                            <label>Help</label>
                        </div>
                    </a>
                </div>
                <div class="col-4">
                    <a href="{{ url('showLogoutForm') }}">
                        <div class="row justify-content-center">
                            <i class="fas fa-user fa-3x white"></i>
                        </div>
                        <div class="row justify-content-center">
                            <label>Account</label>
                        </div>
                    </a>
                </div>
            </div> -->
        </div> 

        <div class="form-group row menu my-5">
            <div class="col-3">
                <a href="{{ url('/home/adult') }}">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/1-adult.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>Adults</label>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="{{ url('/home/couple') }}">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/2-adult.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>Couple</label>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="{{ url('/home/family') }}">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/1-adult-1-kid.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>Family</label>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="{{ url('/home/kids') }}">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/1-kid.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>Kids</label>
                    </div>
                </a>
            </div>
        </div>

        <div class="form-group row menu ">
            <div class="col-3">
                <a href="{{ url('/home/private') }}">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/2-kid.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>Private</label>
                    </div>                
                </a>
            </div>
            <div class="col-3">
                <a href="{{ url('/home/rentals') }}">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/rentals.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>Rentals</label>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="https://www.ripcurl.asia/id/" target="_blank">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/shop.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>Shop</label>
                    </div>
                </a>
            </div>
            <div class="col-3">  
                <!-- <a href="#">
                    <div class="row justify-content-center">
                        <img src="{{ asset('img/more.png') }}" class='main_menu' alt="Logo">
                    </div>
                    <div class="row justify-content-center">
                        <label>More</label>
                    </div>
                </a> -->
            </div>
        </div>
        <div class="form-group fixed-bottom pb-4 pr-4">
            <a href="https://web.whatsapp.com/send?phone=6285100871889&text=" target="_blank" class="btn btn-success float-right" >
                <i class="fab fa-whatsapp fa-lg"></i>&nbsp;
                    connect with us
            </a>
        </div>
    </div>
</div>
@endsection
<div class="overlay"></div>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $(".openbtn").on("click", function(){
        //document.getElementById("mySidepanel").style.width = "200px";
        $("#mySidepanel").addClass("active");
        $('.overlay').fadeIn();
    });

    $(".closebtn").on("click", function(){
        //document.getElementById("mySidepanel").style.width = "0";
        $("#mySidepanel").removeClass("active");
        $('.overlay').fadeOut();
    });
    localStorage.setItem("auth.token","{{ Session::get('auth.currentUser')['access_token'] }}");
    localStorage.setItem("auth.currentUser",'{!! json_encode(Session::get("auth.currentUser")["user"]) !!}');
    
});
</script>