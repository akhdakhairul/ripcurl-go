@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="form-group fixed-top">
    <div class="row red">
        <a class="btn ml-3" href="{{ route('instructor') }}">
            <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
        </a>
        <div class="mx-auto my-auto" style="padding-right:50px;">
            <label class="text-white font-weight-bold">Detail Instructor</label>
        </div>
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col">
            <!-- <h4>Detail Order</h4> -->
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <input type="hidden" name="order_id" id="order_id" value="{{ $order->id }}">
                    <div class="form-group"><label>{{ $order->surf_location }}</label></div>
                    <div class="form-group"><label>Rp. {{ $order->price }}</label></div>
                    <div class="form-group"><label>Product :  {{ $product->surfing }}</label></div>
                    @if($order->status != 'pending')
                    <div class="form-group">
                        <label>Status Order: </label>
                        <label class="font-weight-bold">{{ $order->status }}</label>
                    </div>
                    @endif
                    <div id="map-container" style="width:100%;height:300px;" class="form-group">
                        <div id="map" style="width: 100%; height: 100%" ></div>
                    </div>
                    @if($order->status == 'pending')
                    <div class="form-group">
                        <div class="col">
                            <button type="button" class="btn btn-secondary btn-lg" id="declineOrder" href="#messageModal" data-toggle="modal">Declined</button>
                            <button type="button" class="btn btn-primary btn-lg" id="acceptOrder" href="#messageModal" data-toggle="modal">Accept</button>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assign</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <p class="text-xl-left" id="pesan">Sukses</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal" id="btnOK">OK</button>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&callback=initMap" async defer></script>
    <script>
        function initMap() {
            console.log("{{  explode(',', $order->surf_longitude_latitude)[0] }}")
            var myLatLng = {lat:{{  explode(',', $order->surf_longitude_latitude)[0] }}, lng:{{  explode(',', $order->surf_longitude_latitude)[1] }}};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: myLatLng,
                disableDefaultUI: true
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }

        $("#declineOrder").on("click", function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data = {
                id : "{{ $order->id }}"
            }

            $.ajax({
                type: "POST",
                url: "{{ route('declineOrder') }}",
                data: data,
                success: function(result){
                    $("#pesan").html(result["message"])
                }
            });
        });

        // $("#declineOrder").on("click", function(){
        //     alert('decline');
        // });


        $("#acceptOrder").on("click", function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var data = {
                id : "{{ $order->id }}"
            }

            $.ajax({
                type: "POST",
                url: "{{ route('acceptOrder') }}",
                data: data,
                success: function(result){
                    $("#pesan").html(result["message"])
                }
            });
        });

        $("#btnOK").on("click", function(){
            window.location.href = "{{ url('instructor') }}"
        })
    </script>
    
@endsection
<style>
    .fixed-top{
        position: fixed;
        top:0;
        width: 100%;
    }
    .content{
        margin-top: 10em;
    }

    .anyclass{
        height:175px;
        overflow-y: scroll;
    }
</style>