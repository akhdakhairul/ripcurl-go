@extends('layouts.app')

@section('content')
<div class="form-group fixed-top">
    <div class="row red">
        <div class="mx-auto my-auto" style="padding-right:50px;">
            <label class="text-white font-weight-bold">Order List</label>
        </div>
    </div>
</div>
<div class="container my-5">
   <div class="row">
        <div class="col-2">
            <!-- <h4>Order List</h4> -->
        </div>
        <div class="col-10">
            <div class="float-right">
            <a href="{{ route('logout') }}"
                class="btn btn-primary"
                role="button"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>          
            </div>
        </div>
   </div>
   <!-- <div class="row">
      <div class="col">
         <div class="form-group">
            <label for="date">Date</label>            
            <input type="text" id="date" class="form-control date">
         </div>
      </div>
      <div class="col">
         <div class="form-group">
            <label for="status">Status</label>
            <select id="status" class="form-control" name="status">
               <option value="pending">Pending</option>
               <option value="surfing">Surfing</option>
               <option value="completed">Completed</option>
               <option value="rejected">Rejected</option>
               <option value="canceled">Canceled</option>
            </select>
         </div>
      </div>
   </div> -->
   <div class="row">
        <div class="col">
            <div id="result">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="true">Pending</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="ongoing-tab" data-toggle="tab" href="#ongoing" role="tab" aria-controls="ongoing" aria-selected="false">Ongoing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="false">Completed</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                    <div class="card">
                        <div class="card-body">
                            <div id="pending">
                            @foreach($pendings as $pending)
                                <div class='card'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>{{ $pending->surf_location }}</h5>
                                        <p class='card-text'>Rp. {{ $pending->price }}</p>
                                        <a href= "{{ route('detailOrderInstructor', ['id' => $pending->id ]) }}" class='btn btn-block btn-success'>Detail</a>
                                    </div>
                                </div><br/>
                            @endforeach
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="ongoing" role="tabpanel" aria-labelledby="ongoing-tab">
                    <div class="card">
                        <div class="card-body">
                            <div id="ongoing">
                            @foreach($surfs as $surf)
                                <div class='card'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>{{ $surf->surf_location }}</h5>
                                        <p class='card-text'>Rp. {{ $surf->price }}</p>
                                        <a href= "{{ route('detailOrderInstructor', ['id' => $surf->id ]) }}" class='btn btn-block btn-success'>Detail</a>
                                    </div>
                                </div><br/>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
                    <div class="card">
                        <div class="card-body">
                            <div id="completed">
                            @foreach($completes as $complete)
                                <div class='card'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>{{ $complete->surf_location }}</h5>
                                        <p class='card-text'>Rp. {{ $complete->price }}</p>
                                        <a href= "{{ route('detailOrderInstructor', ['id' => $complete->id ]) }}" class='btn btn-block btn-success'>Detail</a>
                                    </div>
                                </div><br/>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
   </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

        });


        function formatDate(date){
            var d = date.split("/");
            return [d[2], d[1], d[0]].join("-");
        }

        function formatPrice(amt){
            var Amount = parseInt(amt);
            var DecimalSeparator = Number("1.2").toLocaleString().substr(1,1);

            var AmountWithCommas = Amount.toLocaleString();
            var arParts = String(AmountWithCommas).split(DecimalSeparator);
            var intPart = arParts[0];
            var decPart = (arParts.length > 1 ? arParts[1] : '');
            decPart = (decPart + '00').substr(0,2);

            return 'Rp. ' + intPart + DecimalSeparator + decPart;
        }
    </script>
@endsection


<style>
    .fixed-top{
        position: fixed;
        top:0;
        width: 100%;
    }
    .content{
        margin-top: 10em;
    }

    .anyclass{
        height:175px;
        overflow-y: scroll;
    }
</style>
