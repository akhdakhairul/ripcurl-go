@extends('layouts.app')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="form-group fixed-top">
    <div class="row red">
        <a class="btn ml-3" href="{{ url('/order_list') }}/{{ $user_id }}">
            <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
        </a>
        <div class="mx-auto my-auto" style="padding-right:50px;">
            <label class="text-white font-weight-bold">Cancel Order</label>
        </div>
    </div>
</div>
<div class="card mx-3 my-5">
    <div class="card-body">
        <div class="card mb-3">
            <div class="card-body bg-light">
                <div class="row">
                    <div class="col-6">
                        <input type="hidden" name="order_id" id="order_id" value="{{ $order->id }}">
                        <div class="row mx-auto"><p class='font-weight-bold mb-0'>Order No. {{ $order->id }}</p></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col">
                <p class='font-weight-bold'>PRICE</p>
            </div>
            <div class="col">
                <p class='font-weight-bold float-right'>Rp. {{ $order->price }}</p>
            </div>
        </div> -->
    </div>
</div>

<div class="card mb-3 mx-3">
    <input type="hidden" name="surf_latlng" id="surf_latlng" value="{{ $order->surf_longitude_latitude }}" />
    <input type="hidden" name="pickup_latlng" id="pickup_latlng" value="{{ $order->pickup_location }}" />        
    <div id="summary-map-container" style="height:250px">
        <div style="width: 100%; height: 100%" id="summary-map"></div>
    </div>
</div>
<div class="card mb-3 mx-3">
    <div class="card-body">
        <h5 class="font-weight-bold">Location & Time Lesson</h5>
        <hr>
        <div class="row mb-3">
            <div class="col-2 my-auto">
                <i class="far fa-dot-circle fa-lg mb-3"></i>
            </div>
            <div class="col-8">
                YOUR LOCATION
                <div id="your_location" class='font-weight-bold'></div>
            </div>
        </div>
        <div class="row ">
            <div class="col-2 my-auto">
                <i class="fas fa-snowboarding fa-lg"></i>
            </div>
            <div class="col-8">
                SURF LOCATION
                <p class='font-weight-bold'>{{ $order->surf_location }}</p>
            </div>
        </div>
        <hr>
    </div>
</div>
<div class="card mx-3 mb-3 pb-3">
    <div class="card-body">
        <div class="row">
            <div class="col-2">
                <i class="fas fa-calendar-alt fa-lg"></i>
            </div>
            <div class="col-8">
            <p class='font-weight-bold'>10.30 AM - {{ $date }}</p>            
            </div>
        </div>
    </div>
</div>
<div class="card mx-3 mb-3 pb-3">
    <div class="card-body">
        <div class="row">
            <div class="col-2">
                <i class="fas fa-money-check-alt fa-lg"></i>
            </div>
            <div class="col-8">
                <button class="btn btn-danger btn-block" id="cancel" type="button">
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cancel Order</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="" method="post">
            <div class="modal-body"> 
                <p class="text-xl-left" id="pesan">Are You Sure?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-lg" id="submitCancel">Yes</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDB5Snw8xCjawID7hC7w6qssDTlctWYiA4&libraries=places&callback=initialize" async defer></script>
<script type="text/javascript">
function initialize(){
    const surf_latlng = document.getElementById("surf_latlng").value.split(",")
    const surf_latitude = parseFloat(surf_latlng[0]);
    const surf_longitude = parseFloat(surf_latlng[1]);
    const map = new google.maps.Map(document.getElementById('summary-map'), {
        center: {lat: surf_latitude, lng: surf_longitude},
        zoom: 8,
        disableDefaultUI: true,
        styles: [
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#7fc8ed"
                    },
                    {
                        "saturation": 55
                    },
                    {
                        "lightness": -6
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#7fc8ed"
                    },
                    {
                        "saturation": 55
                    },
                    {
                        "lightness": -6
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#83cead"
                    },
                    {
                        "saturation": 1
                    },
                    {
                        "lightness": -15
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#f3f4f4"
                    },
                    {
                        "saturation": -84
                    },
                    {
                        "lightness": 59
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#ffffff"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffffff"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 100
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#bbbbbb"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 26
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffcc00"
                    },
                    {
                        "saturation": 100
                    },
                    {
                        "lightness": -35
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffcc00"
                    },
                    {
                        "saturation": 100
                    },
                    {
                        "lightness": -22
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.school",
                "elementType": "all",
                "stylers": [
                    {
                        "hue": "#d7e4e4"
                    },
                    {
                        "saturation": -60
                    },
                    {
                        "lightness": 23
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]
    });
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    var geocoder = new google.maps.Geocoder();
    directionsDisplay.setMap(map);
    var surf_location = document.getElementById("surf_latlng").value;
    var pickup_location = document.getElementById("pickup_latlng").value;
    calculateAndDisplayRoute(directionsService, directionsDisplay, surf_location, pickup_location);
    geocodeLatLng(geocoder,map,pickup_location);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, surf_location, pickup_location) {
    var surflatlngStr = surf_location.split(',', 2);
    var pickuplatlngStr = pickup_location.split(',', 2);
    var surflatlng = {lat: parseFloat(surflatlngStr[0]), lng: parseFloat(surflatlngStr[1])};
    var pickuplatlng = {lat: parseFloat(pickuplatlngStr[0]), lng: parseFloat(pickuplatlngStr[1])};
    directionsService.route({
      origin: pickuplatlng,  
      destination: surflatlng,  
      travelMode: google.maps.TravelMode["DRIVING"]
    }, function(response, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
}

function geocodeLatLng(geocoder, map,  input) {
    var latlngStr = input.split(',', 2);
    var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {
          map.setCenter(latlng);
          map.setZoom(17);
          marker = new google.maps.Marker({
            position: latlng,
            map: map
          });
          result = results[0].formatted_address;
          console.log(result);
          $("#your_location").html(result)
          //$(".pickup-input").val(result);
          //console.log(results[0].formatted_address);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
    //return result;
}

$("#cancel").on("click", function(){
    $("#exampleModal").modal('show');
})

$("#submitCancel").on("click", function(){

    var data = {
        order_id: $("#order_id").val()
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: "{{ route('cancelOrder') }}",
        data: data,
        success: function(result){
            $("#pesan").html("Success Cancel Order");
            setTimeout(() => {
                window.location.href = "{{ route('home') }}"
            }, 2000);
        }
    });
})
</script>
@endsection