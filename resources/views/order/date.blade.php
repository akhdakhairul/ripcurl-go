@extends('layouts.app')

@section('content')
<div id="app">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-8 ">
                    <div class="card">
                        <div class="card-body">
                            <h5>Your Order: </h5>
                            <h6>{{ $product->surfing }}</h6>
                            <h6>{{ $location_to }} </h6>
                            <h6 class="font-weight-bold">Rp. {{ number_format($product->price, 2, ".", ",") }}</h6>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-8 ">
                    <div class="card">
                        <div class="card-body">
                            <h6>Make sure you are using same email address for megatix account. Your email address: </h6>
                            
                        </div>
                        <form  method="POST" action="{{ route('order') }}">
                            @csrf
                            <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">
                            <input type="hidden" name="surf_location" id="surf_location" value="{{ $location_to }}">
                            <input type="hidden" name="surf_longitude_latitude" id="surf_longitude_latitude" value="{{ $latitude.','.$longitude }}">
                            <input type="hidden" name="user_id" id="user_id" value="1">
                            <input type="hidden" name="price" id="price" value="{{ $product->price }}">
                            <div class="row">

                            </div>
                            <button class="btn waves-effect waves-light" type="submit" id="submitDate" name="action">Continue
                                <i class="fas fa-paper-plane"></i>
                            </button>
                        </form>  
                    </div>      
                </div>    
            </div>

        </div>
    </div>
@endsection