@extends('layouts.app')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="form-group fixed-top">
    <div class="row">
        <a class="btn ml-4 mt-2" href="{{ route('home') }}">
            <i class="fas fa-2x fa-arrow-left fa-lg"></i>
        </a>
    </div>
</div>
<div id="order content">
    <div class="form-group">
        <div id="address-map-container" style="width:100%;height:60%; ">
            <div style="width: 100%; height: 100%" id="address-map"></div>
        </div>    
    </div>
    <input type="hidden" id="base_url" value="{{ URL::to('/') }}">
    <form id="formSubmit" class="form-group mx-4" method="POST" action="javascript:void(0)">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col">
            <div class="row front">
                <input type="hidden" id="address-input" name="address_address" class="form-control map-input">
                <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                <input type="hidden" name="address_longitude" id="address-longitude" value="0" />
                <input type="hidden" name="type" id="type" value="{{ $type }}" />
                <input type="hidden" name="level" id="level" value="{{ $level }}" />
                <input type="hidden" name="price_list_id" id="price_list_id" value="{{ $price_list_id }}" />
            </div>
            <div id="location_list" >
                <div class="row mx-auto">
                    <h5 class="font-weight-bold">Where would you like to surf ?</h5>
                </div>
                <div class="row anyClass mt-3">
                    <div class="col">
                        <div class="btn-group-vertical btn-group-toggle btn-block" id="btnGroup" data-toggle="buttons">
                            @if($level == 0 || $level == 1 || $level == 2 || $level == 4)
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.661366,115.1302333" data-name="Canggu" autocomplete="off" checked="checked">
                                Canggu
                            </label>
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.697374,115.1612504" data-name="Legian" autocomplete="off" checked="checked">
                                Legian
                            </label>
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.722473,115.1641926" data-name="Kuta" autocomplete="off" checked="checked">
                                Kuta
                            </label>
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.7334166,115.2279852" data-name="Serangan" autocomplete="off" checked="checked">
                                Serangan
                            </label>
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.673649999999999,115.26342929999998" data-name="Sanur" autocomplete="off" checked="checked">
                                Sanur
                            </label>
                            @endif                            
                            @if($level == 0 || $level == 2 || $level == 4)
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.6930007,115.1584774" data-name="Seminyak" autocomplete="off" checked="checked">
                                Seminyak
                            </label>
                            @endif
                            @if($level == 0 || $level == 2 || $level == 3)
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.6075052,115.0806717" data-name="Kedungu" autocomplete="off" checked="checked">
                                Kedungu
                            </label>
                            @endif                            
                            @if($level == 0 || $level == 3)
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.5772422,115.399183" data-name="Klotok" autocomplete="off" checked="checked">
                                Klotok
                            </label>
                            @endif
                            @if($level == 0 || $level == 2 || $level == 2 || $level == 3)
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.8102229,115.1011936" data-name="Uluwatu" autocomplete="off" checked="checked">
                                Uluwatu
                            </label>
                            @endif                            
                            @if($level == 0 || $level == 2 || $level == 2 || $level == 3)
                            <label class="btn btn-outline-primary btn-block text-left btn-lg">
                                <input type="radio" name="options" id="option1" value="-8.776348,115.1660958" data-name="Jimbaran" autocomplete="off" checked="checked">
                                Jimbaran
                            </label>
                            @endif                            
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="pickup_location">
                <div class="row mx-auto">
                    <h5 class="font-weight-bold">Where are you departing from?</h5>
                </div>
                <div  class="row mt-3">
                    <input type="text" id="pickup" name="pickup" class="form-control pickup-input">
                    <input type="hidden" id="pickup-latitude" name="pickup_latitude" >
                    <input type="hidden" id="pickup-longitude" name="pickup_longitude">                    
                </div>
                <div class="row mt-3 mb-3">
                    <div class="input-group date">
                        <input type="text" class="form-control" id="pickdate" name="date" placeholder="Pick a Date">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <button id="currentLocation" type="button" class="btn btn-primary btn-block">Use Your Current Location</button>
                </div>
            </div>
            <!-- <div class="row mb-5">
                <button type="button" class="btn btn-light btn-block" id="select-via-map">
                    <i class="far fa-map"></i>&nbsp;
                    SELECT VIA MAP
                </button>
            </div> -->
            <div class="row justify-content-center mt-2">
                <div class="col-12">                
                        <!-- <a href="{{ route('pickdate', ['price_list_id' => $price_list_id, 'type'=> $type ]) }}" class="btn btn-danger btn-block" id="btnOrder" role="button">
                            Order href="#paymentModal" data-toggle="modal"
                        </a> -->
                        <button id="submitSurf" class="btn btn-danger btn-block" type="button">
                            Next
                        </button>

                        <button id="submitOrder" class="btn btn-danger btn-block" type="submit">
                            Order
                        </button>
<!--                    <button class="mt-c-btn-buy-now btn btn-danger btn-block" href="#paymentModal" type="submit">
                            Order
                        </button>     -->
                </div>
            </div>        
        </div>
    </form>
    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
            <label id="pesan" for="">Order Success, Continue with payment</label>
            <button class="mt-c-btn-buy-now btn btn-danger btn-block" id="bayar" type="button" 
                data-mt-source="https://megatix.com.au/api/v2/accounts/token-redirect?next=/white-label/surf&token={{ Session::get('auth.currentUser')['access_token'] }}">
                Continue
            </button>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-lg" href="{{ url('home') }}" data-dismiss="modal" id="btnOK">Return To Home</button>
            </div>
            </div>
        </div>
    </div>
 </div> 
@endsection
@section('scripts')
@parent
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDB5Snw8xCjawID7hC7w6qssDTlctWYiA4&libraries=places&callback=initialize" async defer></script>
    <script src="/js/mapInput.js"></script>
    <script>
        $("#pickup_location").hide();
        $("#submitOrder").hide();
        $("#submitDate").hide();
        $("#submitSurf").hide();
        $(".date").hide();

        $("#submitSurf").on("click", function(){
            $("#submitSurf").hide();
            //$("#submitOrder").show();
            $("#location_list").hide();
            $("#pickup_location").show();
        });

        $("#formSubmit").on("submit", function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: "{{ route('order') }}",
                data: $("#formSubmit").serialize(),
                success: function(result){
                    // $("#paymentModal").modal("show");
                    // $("#pesan").html(result["message"]);
                    window.location.href = "{{ route('pending') }}";
                },
                error: function(result){
                    console.log(result);
                }
            });

        });
        var today = new Date();
        $("#pickdate").datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            startDate: today,
            endDate: new Date(new Date().setDate(new Date().getDate() + 14))
        })

        $("#pickdate").datepicker()
            .on("changeDate", function(){
                $("#submitOrder").show();
            })

        // $("#submitOrder").on("click", function(){
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     $.ajax({
        //         type: "POST",
        //         url: "{{ route('order') }}",
        //         data: $("#formSubmit").serialize(),
        //         success: function(result){
        //             $("#paymentModal").modal("show");
        //             $("#pesan").html(result["message"]);
                    
        //         }
        //     });
        //     console.log($("#formSubmit").serialize());
        // });

        // $("#btnGroup .btn").on("click", function(event){
        //     console.log($(this).find('input').val());
        //     //var val = $(this).find('input').val();
        // })
    </script>
@endsection

<style>
    .fixed-top{
        position: fixed;
        top:0;
        width: 100%;
    }
    .content{
        margin-top: 10em;
    }

    .anyclass{
        height:20%;
        overflow-y: scroll;
    }
</style>