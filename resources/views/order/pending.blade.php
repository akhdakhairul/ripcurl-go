@extends('layouts.app')
@section('content')
<div class="form-group fixed-top">
    <div class="row red">
        <a class="btn ml-3" href="{{ route('home') }}">
            <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
        </a>
        <div class="mx-auto my-auto" style="padding-right:50px;">
            <label class="text-white font-weight-bold">ORDER STATUS</label>
        </div>
    </div>
</div>
<div class="form-group content">
    <div class="col">
        <div class="row justify-content-center">
            <img src="{{ asset('img/surfer.png') }}" alt="" height="200px" >
        </div>
        <div class="row my-4 justify-content-center">
            <h6 class="font-weight-bold">
                IN PROGRESS...
            </h6>
        </div>
        <div class="row justify-content-center">
            <p>Please kindly wait, we are still processing your order</p>
        </div>
        <!-- <div class="row justify-content-center">
            <a class="btn btn-primary" href="{{ route('home') }}" >
                Home
            </a>
        </div> -->
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
$(document).ready(function(){
    setTimeout(() => {
        window.location.href = "{{ route('home') }}"
    }, 2000);
})
</script>
@endsection

<style>
    .fixed-top{
        position: fixed;
        top:0;
        width: 100%;
    }
    .content{
        margin-top: 15em;
    }
</style>