@extends('layouts.app')
@section('content')
    <div class="form-group">
        <div class="col">
            <div class="row justify-content-center">
                <h6 class="font-weight-bold ">ORDER STATUS</h6>
            </div>
            <hr>
            <div class="row justify-content-center">
                <p>Order Success Proceed to payment</p>
            </div>
            <div class="row justify-content-center">
                <button class="mt-c-btn-buy-now" type="button" data-mt-source="https://megatix.co.id/white-label/surf">Purchase</button>
            </div>
            <div class="row justify-content-center">
                <p>Or Back to Home</p>
            </div>
            <div class="row justify-content-center">
                <a class="btn btn-primary" id="btnOrder" href="{{ route('home') }}">
                    Home
                </a>
            </div>
        </div>
    </div>
    <script defer async src="https://megatix.co.id/js/widgets/buy-now-plugin.js"></script>
@endsection