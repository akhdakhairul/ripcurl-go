@extends('layouts.app')
@section('content')
<div id="mySidepanel" class="sidepanel">
    <a href="javascript:void(0)" class="closebtn">&times;</a>
    <a href="{{ route('adminOrder') }}">Order</a>
    <a href="{{ route('adminInstructor') }}">Instructor</a>
    <a href="{{ route('adminUser') }}">User</a>
    <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a> 
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
<div id="session"></div>
<div class="container-fluid" id="dashboard">
    <div class="form-group">
        <div class="row admin-top">
            <button class="openbtn">
                <i class="fas fa-align-justify white"></i>   
            </button> 
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white">GO SURF</label>
            </div>
        </div>
        <form action="{{ route('adminOrder') }}" method="get">    
            <div class="row mt-2">
                <div class="col">
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="status" name="status">
                            <option value="pending" selected>Pending</option>
                            <option value="assigned">Assigned</option>
                            <option value="completed">Completed</option>
                            <option value="rejected">Rejected</option>
                            <option value="canceled">Canceled</option>
                        </select>
                    </div>        
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <input type="submit" value="submit" class="btn btn-block btn-primary"/>
                </div>
            </div>
        </form>
        <br/>
        <div class="row">
            <div class="col">
                <div id="result">
                    @foreach($orders as $order)
                        <div class='card'>
                            <div class='card-body'>
                                <h5 class='card-title'>{{ $order->surf_location }}</h5>
                                <p class='card-text'>Rp. {{ number_format($order->price, 2) }}</p>
                                <p class="card-text">{{ $order->user->name }}</p>
                                <a href= "{{ route('detailOrder', ['id' => $order->id ]) }}" class='btn btn-block btn-success'>Detail</a>
                            </div>
                        </div><br/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            var stat = getUrlParameter("status")
            if (stat != "undefined"){
                $(".status").val(stat).trigger("change");
            }
        });

        $(".status").select2({
            placeholder: "Pilih",
            theme: "bootstrap",
            minimumResultsForSearch: -1
        });

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };
        function loadData(status){
            $.ajax({
                url: "/getOrderInstructor/" + status,
                type: "GET",
                dataType: "json",
                success: function(result){
                    $("#result").empty();
                    var card = "";
                    for(i=0; i< result.length; i++){
                        console.log(result[i]["id"])
                        var id = result[i]["id"]
                        console.log(id)

                        card = "<div class='card'>" +
                            "<div class='card-body'>" +
                                "<h5 class='card-title'>"+ result[i]["surf_location"] +"</h5>" +
                                "<p class='card-text'>"+ formatPrice(result[i]["price"]) +"</p>" +
                                "<a id='iki' href= '#' class='btn btn-block btn-primary'>Detail</a>" +
                            "</div>" +
                        "</div><br/>"
                        $("#result").append(card);
                    }
                }
            });
        }

        function formatDate(date){
            var d = date.split("/");
            return [d[2], d[1], d[0]].join("-");
        }

        function formatPrice(amt){
            var Amount = parseInt(amt);
            var DecimalSeparator = Number("1.2").toLocaleString().substr(1,1);

            var AmountWithCommas = Amount.toLocaleString();
            var arParts = String(AmountWithCommas).split(DecimalSeparator);
            var intPart = arParts[0];
            var decPart = (arParts.length > 1 ? arParts[1] : '');
            decPart = (decPart + '00').substr(0,2);

            return 'Rp. ' + intPart + DecimalSeparator + decPart;
        }
    </script>
@endsection
<div class="overlay"></div>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".openbtn").on("click", function(){
            //document.getElementById("mySidepanel").style.width = "200px";
            $("#mySidepanel").addClass("active");
            $('.overlay').fadeIn();
        });

        $(".closebtn").on("click", function(){
            //document.getElementById("mySidepanel").style.width = "0";
            $("#mySidepanel").removeClass("active");
            $('.overlay').fadeOut();
        });
    })
</script>

<style>
.sidepanel{
    background-color:#105e62 !important;
}
.openbtn{
    background-color: #b5525c !important;
}
</style>