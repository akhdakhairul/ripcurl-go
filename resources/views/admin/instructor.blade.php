@extends('layouts.app')
@section('content')
<div id="mySidepanel" class="sidepanel">
    <a href="javascript:void(0)" class="closebtn">&times;</a>
    <a href="{{ route('adminOrder') }}">Order</a>
    <a href="{{ route('adminInstructor') }}">Instructor</a>
    <a href="{{ route('adminUser') }}">User</a>
    <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a> 
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
<div id="session"></div>
<div class="container-fluid" id="dashboard">
    <div class="form-group">
        <div class="row admin-top">
            <button class="openbtn">
                <i class="fas fa-align-justify white"></i>   
            </button> 
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white">INSTRUCTOR</label>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col ml-2">
                <label for="user">&nbsp;</label>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col ml-2">
                <button id="add_instructor" class="btn btn-primary" href='#exampleModal' id='btnAssign'  data-toggle='modal' data-backdrop='static' data-keyboard='false'><i class='fa fa-fw fa-user'></i> Add Instructor</button>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col ml-2">
                <table id="instructor_table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="150px">Aksi</th>
                            <th width="800px">Name</th>
                            <th width="800px">Phone</th>
                            <th width="800px">Email</th>
                            <th width="800px">User Picture</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="modal_title"></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body"> 
                <input type="hidden" name="identifier" id="identifier">
                <p class="text-xl-left" id="instructor"></p>
                <div id="add">
                    <form class="form-signin" action="{{ route('adminInstructorStore') }}" method="POST" enctype="multipart/form-data">
                        @csrf            
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="email">E-Mail Address</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
                        </div>

                        <div class="form-group">
                            <label for="phone">{{ __('Phone') }}</label>
                            <input id="phone" type="text" pattern="\d*" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" maxlength="12" value="{{ old('phone') }}" name="phone" placeholder="phone" required>
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div> 

                        <div class="form-group">
                            <label for="">{{ __('Foto') }}</label>
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" name="profile_image" accept=".png, .jpg, .jpeg" required />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add Instructor') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="edit">
                    <form class="form-signin" action="{{ route('adminInstructorUpdate') }}" method="POST" enctype="multipart/form-data">
                        @csrf            
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="edit_name" name="edit_name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" required autofocus>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="email">E-Mail Address</label>
                            <input id="edit_email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="edit_email" placeholder="Email" readonly>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label for="phone">{{ __('Phone') }}</label>
                            <input id="edit_phone" type="text" pattern="\d*" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" maxlength="12" name="edit_phone" placeholder="phone" required>
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div> 

                        <div class="form-group">
                            <label for="">{{ __('Foto') }}</label>
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="edit_imageUpload" name="edit_profile_image" accept=".png, .jpg, .jpeg" />
                                    <label for="edit_imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="edit_imagePreview" ></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update Instructor') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="delete">
                    <form action="{{ route('adminInstructorDelete') }}" method="get">
                        @csrf
                        <input type="hidden" name="id" id="id" />
                        <div class="form-group">
                            <label for="phone">{{ __('Are You Sure?') }}</label>
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-danger">
                                    {{ __('Delete') }}
                                </button>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<div class="overlay"></div>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#edit_imageUpload").change(function() {
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#edit_imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#edit_imagePreview').hide();
                    $('#edit_imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#edit_imageUpload").change(function() {
            readURL(this);
        });


        var table = $("#instructor_table").DataTable( {
            ajax: {
                url: "{{ route('getAllInstructor') }}",
                dataSrc: "data"
            },
            dom: 't<"dataTables_footer" lfpi>r',
            bFilter: false,
            info: false,
            ordering: false,
            bLengthChange: false,
            columns: [ //<button type='button' class='btn btn-default btn-xs btn-danger' href='#exampleModal' id='delete_instructor'  data-toggle='modal'  data-backdrop='static' data-keyboard='false' data-toggle='tooltip' data-placement='bottom' title='delete' style='margin: 0 3px 5px !important;'><i class='fa fa-fw fa-trash'></i></button>
                { defaultContent: "<button type='button' class='btn btn-xs btn-primary' href='#exampleModal' id='edit_instructor'  data-toggle='modal'  data-backdrop='static' data-keyboard='false' data-toggle='tooltip' data-placement='bottom' title='edit' style='margin: 0 3px 5px !important;'><i class='fa fa-fw fa-edit'></i></button>" },
                { data: 'name' },
                { data: 'phone' },  
                { data: 'email' },
                { 
                    data: 'user_picture',
                    render: function(data){
                        return '<img src="data:image/png;base64, '+ data +'" width="150px" alt="No Image" />'
                    }
                }
            ]
        });
        $(".openbtn").on("click", function(){
            //document.getElementById("mySidepanel").style.width = "200px";
            $("#mySidepanel").addClass("active");
            $('.overlay').fadeIn();
        });

        $(".closebtn").on("click", function(){
            //document.getElementById("mySidepanel").style.width = "0";
            $("#mySidepanel").removeClass("active");
            $('.overlay').fadeOut();
        });

        $("#instructor_table").on("click", "#edit_instructor", function(){
            var tableData = table.row($(this).parents('tr')).data();
            $("#modal_title").html("<h5>Edit Instructor</h5>");
            console.log(tableData);
            $("#edit_email").val(tableData["email"]);
            $("#edit_name").val(tableData["name"]);
            $("#edit_phone").val(tableData["phone"]);
            //style="background-image: url(http://i.pravatar.cc/500?img=7);"
            $('#edit_imagePreview').css('background-image', 'url("data:image/png;base64, ' + tableData["user_picture"] +'")');
            $("#add").hide();
            $("#edit").show();
            $("#delete").hide();
        })

        $("#instructor_table").on("click", "#delete_instructor", function(){
            var tableData = table.row($(this).parents('tr')).data();
            $("#modal_title").html("<h5>Delete Instructor</h5>");
            $("#id").val(tableData["id"]);
            $("#add").hide();
            $("#edit").hide();
            $("#delete").show();
        })

        $("#add_instructor").on("click", function(){
            $("#modal_title").html("<h5>Add Instructor</h5>");
            $("#add").show();
            $("#edit").hide();
            $("#delete").hide();
        });
    });

</script>

<style>
.sidepanel{
    background-color:#105e62 !important;
}
.openbtn{
    background-color: #b5525c !important;
}
 
</style>