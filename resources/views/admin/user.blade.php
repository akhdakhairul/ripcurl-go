@extends('layouts.app')
@section('content')
<div id="mySidepanel" class="sidepanel">
    <a href="javascript:void(0)" class="closebtn">&times;</a>
    <a href="{{ route('adminOrder') }}">Order</a>
    <a href="{{ route('adminInstructor') }}">Instructor</a>
    <a href="{{ route('adminUser') }}">User</a>
    <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a> 
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
<div id="session"></div>
<div class="container-fluid" id="dashboard">
    <div class="form-group">
        <div class="row admin-top">
            <button class="openbtn">
                <i class="fas fa-align-justify white"></i>   
            </button> 
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white">USER</label>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col ml-2">
                <label for="user">&nbsp;</label>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col ml-2">
                <table id="user_table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="800px">Name</th>
                            <th width="800px">Phone</th>
                            <th width="800px">Email</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
<div class="overlay"></div>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var table = $("#user_table").DataTable( {
            ajax: {
                url: "{{ route('getAllUser') }}",
                dataSrc: "data"
            },
            dom: 't<"dataTables_footer" lfpi>r',
            bFilter: false,
            info: false,
            ordering: false,
            bLengthChange: false,
            columns: [
                //{ defaultContent: "<button type='button' class='btn btn-default btn-xs btn-danger' href='#exampleModal' id='btnAssign'  data-toggle='modal'  data-backdrop='static' data-keyboard='false' style='margin: 0 3px 5px !important;'><i class='fa fa-fw fa-trash'></i> Deactivate</button>" },
                { data: 'name' },
                { data: 'phone' },  
                { data: 'email' }
            ]
        });
        $(".openbtn").on("click", function(){
            //document.getElementById("mySidepanel").style.width = "200px";
            $("#mySidepanel").addClass("active");
            $('.overlay').fadeIn();
        });

        $(".closebtn").on("click", function(){
            //document.getElementById("mySidepanel").style.width = "0";
            $("#mySidepanel").removeClass("active");
            $('.overlay').fadeOut();
        });
    })
</script>

<style>
.sidepanel{
    background-color:#105e62 !important;
}

.openbtn{
    background-color: #b5525c !important;
}
</style>