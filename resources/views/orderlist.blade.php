@extends('layouts.app')

@section('content')
<div class="container">
    <div class="form-group fixed-top">
        <div class="row red">
            <a class="btn ml-3" href="{{ route('home') }}">
                <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
            </a>
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white font-weight-bold">ORDER STATUS</label>
            </div>
        </div>
    </div>
    <div class="form-group content">
        <div class="col">
            <div id="result">
                @foreach($order_list as $order)
                    <div class='card'>
                        <div class='card-body'>
                            <h5 class='card-title'>{{ $order->surf_location }}</h5>
                            <p class='card-text'>Rp. {{ $order->price }}</p>
                            @if ($order->status == "assigned")
                            <a href= "{{ route('summary', ['order_id' => $order->id ]) }}" class='btn btn-block btn-success'>Detail</a>
                            @endif
                            
                            @if($order->status == "pending")
                            <p class='card-text'>Please Wait!. We are processing your order</p>
                            <a href= "{{ route('cancel', ['order_id' => $order->id ]) }}" class='btn btn-block btn-danger'>Cancel Order</a>
                            @endif

                            @if($order->status == "expired")
                            <p class='card-text'>Order Expired!</p>
                            @endif

                            @if($order->status == "completed")
                            <p class='card-text'>Order Completed!</p>
                            @endif

                            @if($order->status == "rejected")
                            <p class='card-text'>Not Available!</p>
                            @endif


                            @if($order->status == "canceled")
                            <p class='card-text'>Order Canceled</p>
                            @endif
                        </div>
                    </div><br/>
                @endforeach
            </div>
        </div>
    </div>
    </div>    
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cancel Order Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="" method="post">
            <div class="modal-body"> 
                <input type="text" name="order_id" id="order_id"/>
                <p class="text-xl-left" id="pesan">Are You Sure?</p>
            </div>
            <div class="modal-footer">            
                <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary btn-lg" id="submitCancel">Yes</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
<style>
    .fixed-top{
        position: fixed;
        top:0;
        width: 100%;
    }
    .content{
        margin-top: 5em;
    }
</style>
