@extends('layouts.app')
@section('content')
    <div class="container payment">
        <div class="row justify-content-center title">
            <div class="col-md-8 ">
                <p>PAYMENT</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a href="#">
                    <img src="{{ asset('img/payment_trf.png') }}" class="payment_menu" alt="">
                    <p>Transfer</p>
                </a>
            </div>
            <div class="col">
                <a href="#">
                    <img src="{{ asset('img/payment_cc.png') }}" class="payment_menu" alt="">
                    <p>Credit Card</p>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <a href="#">
                    <img src="{{ asset('img/payment_doku.png') }}" class="payment_menu" alt="">
                    <p>Doku Wallet</p>
                </a>
            </div>
            <div class="col">
                <a href="#">
                    <img src="{{ asset('img/payment_others.png') }}" class="payment_menu" alt="">
                    <p>Others</p>
                </a>
            </div>
        </div>
    </div>
@endsection