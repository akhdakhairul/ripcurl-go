@extends('layouts.app')

@section('content')
<?php header('Access-Control-Allow-Origin: *'); ?>
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container">
    <div class="row my-5"></div>
    <div class="row text-center my-5">
      <div class="col">
          <img src="{{ asset('img/logo.png') }}" alt="Logo" width="200px">
      </div>  
    </div>
    <div class="row my-3"></div>
    <form id="form_login" class="form-signin" method="POST" action="{{ route('check_login') }}"> 
      @csrf
        <div class="form-label-group">
          <input type="email" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email address" required autofocus>
          @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
          <label for="email">Email Address</label>
          
        </div>

        <div class="form-label-group">
          <input type="password" id="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  placeholder="Password" required>
          @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
          <label for="password">Password</label>
        </div>

        <div class="mb-3">
          <div class="row">
            <div class="col-3">
              <label class="switch">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <span class="slider round"></span>
              </label>
            </div>
            <div class="col-4 my-auto">
              <label class="form-check-label" for="remember">Remember Me</label>
            </div>
            <div class="col-2">
              @if (Route::has('password.request'))
                  <a class="btn btn-link" href="{{ route('password.request') }}">
                      {{ __('Forgot Password?') }}
                  </a>
              @endif
            </div>
          </div>
        </div>
          <button class="btn btn-lg btn-outline-dark btn-block text-uppercase" type="submit">
              <span style="font-size:x-large;font-weight:bold">
                Sign in
              </span>
          </button>
          
            <!-- <a href="{{ route('showRegister') }}" class="btn btn-lg btn-success btn-block text-uppercase" role="button">
                Register
            </a>                             -->
        </form>
        <div class="row">
          <div class="col">
            Don't Have An Account? <a href="{{ route('registration') }}" class="btn btn-link">
                  Sign Up
              </a>
          </div>
        </div>
    </div>
  </div>
    <!-- <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign in with Google</button>
        <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign in with Facebook</button> -->
         
@endsection



<style>
:root {
  --input-padding-x: 1.5rem;
  --input-padding-y: .75rem;
}


body {
  background: #cd5c5c;
  background-image: url('img/login_background.jpg');
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.card-signin {
  border: 0;
  border-radius: 1rem;
  box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
  background: transparent;
}

.card-signin .card-title {
  margin-bottom: 2rem;
  font-weight: 300;
  font-size: 1.5rem;
}

.card-signin .card-body {
  padding: 2rem;
}

.form-signin {
  width: 100%;
}

.form-signin .btn {
  font-size: 80%;
  letter-spacing: .1rem;
  font-weight: bold;
  padding: 1rem;
  transition: all 0.2s;
}

.form-label-group {
  position: relative;
  margin-bottom: 1rem;
}

.form-label-group input {
  height: auto;
  opacity:0.5;
  
}

.form-control{
  color: black !important;
}

.form-control:focus {
  border-radius:0px !important;
  opacity:0.5;
  border-color:#c55019 !important;
}

.form-label-group>input,
.form-label-group>label {
  padding: var(--input-padding-y) var(--input-padding-x);
}

.form-label-group>label {
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  margin-bottom: 0;
  /* Override default `<label>` margin */
  line-height: 1.5;
  color: black;
  transition: all .1s ease-in-out;
}

.form-label-group input::-webkit-input-placeholder {
  color: transparent;
}

.form-label-group input:-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-moz-placeholder {
  color: transparent;
}

.form-label-group input::placeholder {
  color: transparent;
  background:transparent;
}

.form-label-group input:not(:placeholder-shown) {
  padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
  padding-bottom: calc(var(--input-padding-y) / 3);
}

.form-label-group input:not(:placeholder-shown)~label {
  padding-top: calc(var(--input-padding-y) / 3);
  padding-bottom: calc(var(--input-padding-y) / 3);
  font-size: 12px;
  color: black;
}

.form-label-group label.active{
  opacity:0;
}

.btn-google {
  color: white;
  background-color: #ea4335;
}

.btn-facebook {
  color: white;
  background-color: #3b5998;
}

/* Fallback for Edge
-------------------------------------------------- */

@supports (-ms-ime-align: auto) {
  .form-label-group>label {
    display: none;
  }
  .form-label-group input::-ms-input-placeholder {
    color: black;
  }
}

/* Fallback for IE
-------------------------------------------------- */

@media all and (-ms-high-contrast: none),
(-ms-high-contrast: active) {
  .form-label-group>label {
    display: none;
  }
  .form-label-group input:-ms-input-placeholder {
    color: black;
  }
}

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

@section('scripts')
@parent
<script>
  $(document).ready(function(){
    $("#error_alert").hide();
    // var data = 'email=akhda.khairul@gmail.com&password=the gunners'
    // var url = 'https://megatix.co.id/api/v2/accounts/login';
    // var xhr = createCORSRequest('POST', url);
    // xhr.send(data);
    //makeCorsRequest();
    localStorage.setItem("name", "laravel-nepal");
  });
  // $("#form_login").on("submit", function(e){
  //     e.preventDefault();
  //     var data = {
  //       "email": $("#email").val(),
  //       "password": $("#password").val()
  //     }
  //     $.ajaxSetup({
  //         headers: {
  //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
  //             'Access-Control-Allow-Origin': "*"
  //         }
  //     });

  //     var register = {
  //       "name": $("#email").val(),
  //       "email": $("#email").val(),
  //       "password": $("#password").val(),
  //     }

  //     $.ajax({
  //       type: "POST",
  //       url: "{{ route('login') }}",
  //       data: data,
  //       success: function(){
  //         $.ajax({
  //           url: "https://megatix.co.id/api/v2/accounts/login",
  //           data: JSON.stringify(data),
  //           method: 'POST',
  //           contentType: "application/json",
  //           crossDomain: true,
  //           dataType: "json",
  //           success: function(data){
  //             window.location = "/home"
  //             /*
  //               save this megatix data to session
  //             */
  //             console.log(data)
  //           },
  //           error: function(){
  //             $("#error_alert").show();
  //             $("#error_message").html("Incorrect Username Or Password");
  //           }
  //         });
  //       },
  //       error:function(error){
  //         console.log(error);
  //         $.ajax({
  //           type: "POST",
  //           url: "{{ route('register') }}",
  //           data : register,
  //           success: function(result) {
  //             window.location = "/home"
  //           },
  //           error: function(error){
  //             window.location = "/login"
  //           }
  //         //window.location = "/login"
  //         });
  //       }
  //     });   
  //});


  function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // XHR for Chrome/Firefox/Opera/Safari.
      xhr.open(method, url, true);
      console.log("cors supported")
    } else if (typeof XDomainRequest != "undefined") {
      // XDomainRequest for IE.
      xhr = new XDomainRequest();
      xhr.open(method, url);
      console.log("cors supported")
    } else {
      // CORS not supported.
      xhr = null;
      console.log("cors not supported")
    }
    return xhr;
  }
  var data = {
    "email": $("#email").val(),
    "password": $("#password").val()
  }
  function makeCorsRequest() {
  // This is a sample server that supports CORS.
    var url = 'https://megatix.co.id/api/v2/accounts/login';

    var xhr = createCORSRequest('POST', url);
    if (!xhr) {
      alert('CORS not supported');
      return;
    }

    //xhr.setRequestHeader('Content-type', 'application/json');
    // Response handlers.
    // xhr.onload = function() {
    //   var text = xhr.responseText;
    //   //var title = getTitle(text);
    //   alert('Response from CORS request to ' + url + ': ' + text);
    // };

    // xhr.onerror = function() {
    //   alert('Woops, there was an error making the request. ' + xhr.statusText);
    // };
    var data = 'email=akhda.khairul@gmail.com&password=the gunners'
    xhr.send(data);
  }

  /**
   * 
   * 
   * 
   */
</script>
@endsection