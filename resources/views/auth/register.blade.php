@extends('layouts.app')
@section('navbar')
<nav class="navbar navbar-expand-md gray shadow-sm">
  <div class="container">
    <div>
      <a class="navbar-brand" href="{{ url('/register/show') }}">
        <i class="fas text-black fa-chevron-left fa-lg"></i>
      </a>
    </div>
    <div class="mx-auto">
      <h5 class="text-black my-auto">REGISTRATION</h5>
    </div>
  </div>
</nav>
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">`
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin">
                <div class="card-body">
                    <form id="form_signup" class="form-signin" method="POST" action="{{ route('register_user') }}" >
                        @csrf
                        <div class="form-label-group text-center">
                          <h5>CREATE YOUR ACCOUNT</h5>
                        </div>
                        <div class="form-label-group">
                            <input type="text" id="name" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name') }}" required autofocus>
                            <label for="name">Name</label>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-label-group">
                            <input type="email" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email address" value="{{ old('email') }}" required autofocus>
                            <label for="email">Email Address</label>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-label-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <label for="password">Password</label>
                        </div>

                        <div class="form-label-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
                            <label for="password-confirm">Confirm Password</label>
                        </div>

                        <div class="form-label-group">
                            <input id="phone" type="text" pattern="\d*" maxlength="12" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" placeholder="Phone" required>
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                            <label for="phone">Phone</label>
                        </div>  

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        
                        <!-- <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button type="button" class="btn btn-primary btn-lg btn-block">
                                <i class="fa fa-facebook"></i> &nbsp;
                                    Continue with Facebook
                                </button>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button type="button" class="btn btn-primary btn-lg btn-block">
                                    <i class="fa fa-google"></i> &nbsp;
                                    Continue with Google
                                </button>
                            </div>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
  // $("#form_signup").on("submit", function(){
  //   $.ajaxSetup({
  //       headers: {
  //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //       }
  //   });
  //   var first_name, last_name;
  //   var name = $("#name").val();
  //   var name_arr = name.split(" ", 2)
  //   if (name_arr.length < 2){
  //     first_name = name_arr[0]; 
  //     last_name = name_arr[0];
  //   }else{
  //     first_name = name_arr[0];
  //     last_name = name_arr[1];
  //   }
  //   var data = {
  //     email: $("#email").val(),
  //     password: $("#password").val(), 
  //     password_confirmation: $("#password-confirm").val(),
  //     first_name: first_name,
  //     last_name: last_name,
  //     phone: $("#phone").val(),
  //     terms: true
  //   };
  //   console.log(JSON.stringify(data));
  //   console.log($("#form_signup").serialize())
  //   $.ajax({
  //     method: "POST",
  //     url: "https://megatix.co.id/api/v2/accounts/register",
  //     headers: {'Access-Control-Allow-Origin': '*'},
  //     data : JSON.stringify(data),
  //     contentType: 'application/json',
  //     crossDomain: true,
  //     dataType: 'json',
  //     success: function(result) {
  //       console.log(result);
  //       //window.location = "/home"
  //     },
  //     error: function(jqXHR, textStatus, errorThrown){
  //       console.log("error ")
  //       console.log(jqXHR, textStatus, errorThrown)
  //     }
  //   });

  //   function register(){
  //     $.ajax({
  //     type: "POST",
  //     url: "{{ route('register') }}",
  //     data : $("#form_signup").serialize(),
  //     success: function(result) {
  //       window.location = "/home"
  //     }
  //   })
  //   }
  // })
</script>

@endsection
<style>
:root {
  --input-padding-x: 1.5rem;
  --input-padding-y: .75rem;
}

input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
 
input[type="number"] {
    -moz-appearance: textfield;
}

body {
    background-image: url('/img/bells-beach.jpg');
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

.card-signin {
  border: 0;
  box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}

.card-signin .card-title {
  margin-bottom: 2rem;
  font-weight: 300;
  font-size: 1.5rem;
}

.card-signin .card-body {
  padding: 2rem;
}

.form-signin {
  width: 100%;
}

.form-signin .btn {
  font-size: 80%;
  letter-spacing: .1rem;
  font-weight: bold;
  padding: 1rem;
  transition: all 0.2s;
}

.form-label-group {
  position: relative;
  margin-bottom: 1rem;
}

.form-label-group input {
  height: auto;
}

.form-label-group>input,
.form-label-group>label {
  padding: var(--input-padding-y) var(--input-padding-x);
  position:relative;
}

.form-label-group>label {
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  margin-bottom: 0;
  /* Override default `<label>` margin */
  line-height: 1.5;
  color: #495057;
  border: 1px solid transparent;
  transition: all .1s ease-in-out;
}

.form-label-group input::-webkit-input-placeholder {
  color: transparent;
}

.form-label-group input:-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-moz-placeholder {
  color: transparent;
}

.form-label-group input::placeholder {
  color: transparent;
}

.form-label-group input:not(:placeholder-shown) {
  padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
  padding-bottom: calc(var(--input-padding-y) / 3);
}

.form-label-group input:not(:placeholder-shown)~label {
  padding-top: calc(var(--input-padding-y) / 3);
  padding-bottom: calc(var(--input-padding-y) / 3);
  font-size: 12px;
  color: #777;
}

.btn-google {
  color: white;
  background-color: #ea4335;
}

.btn-facebook {
  color: white;
  background-color: #3b5998;
}

/* Fallback for Edge
-------------------------------------------------- */

@supports (-ms-ime-align: auto) {
  .form-label-group>label {
    display: none;
  }
  .form-label-group input::-ms-input-placeholder {
    color: #777;
  }
}

/* Fallback for IE
-------------------------------------------------- */

@media all and (-ms-high-contrast: none),
(-ms-high-contrast: active) {
  .form-label-group>label {
    display: none;
  }
  .form-label-group input:-ms-input-placeholder {
    color: #777;
  }
}
</style>