@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <h5 class="text-center">Account</h5>
            <div class="card">
                <div class="card-body">
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary">
                        Logout
                    </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection