@extends('layouts.app')

@section('content')
<div class="container">
    @if($type=='kids')
    <div class="form-group fixed-top">
        <div class="row red">
            <a class="btn ml-3" href="{{ route('home') }}">
                <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
            </a>
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white font-weight-bold">Choose Product</label>
            </div>
        </div>
    </div>
    <div class="form-group content">
        <div class="card mt-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-5">
                        <img src="{{ asset('img/level/little-surfer.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="col-7">
                        <h6 class="card-title font-weight-bold">LITTLE RIPPER - UNDER 13YR PROGRAM</h6>
                        <p class="card-text">Give your grom the best head start to become a little ripper with a course designed specifically for kids. With one-on-one tuition with one of our coaches your kid will get answers to all their surfing questions and always be under close supervision. It also allows for a lesson to be adapted specifically for your child’s ability. The course is all about getting your little ones stoked to surf, through confidence building, skill improvement and having lots of fun.</p>
                    </div>
                </div>
            </div>
        </div>
        <p class="card-text text-primary">* All prices are subject to an additional 10% government tax</p>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Lessons 1</h6>
                        <!-- <h6>Rp 715,000* for 1 hr</h6> -->
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/kids/12') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Lessons 3</h6>
                        <!-- <h6>Rp 1,815,000* for 3 hr</h6> -->
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/kids/13') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Lessons 5</h6>
                        <!-- <h6>Rp 2,2860,000* for 5 hr</h6> -->
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/kids/14') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($type=='adult')
    <div class="form-group fixed-top">
        <div class="row red">
            <a class="btn ml-3" href="{{ route('home') }}">
                <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
            </a>
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white font-weight-bold">Choose Product</label>
            </div>
        </div>
    </div>
    
    <div class="form-group content">
        <p class="card-text text-primary">* All prices are subject to an additional 10% government tax</p>
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <div class="row">
                        <div class="col-5">
                            <img src="{{ asset('img/beach-surfer.jpg') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-7">
                            <h5 class="mb-0">
                                <a class="btn btn-link" >
                                LEVEL 1 <br/>
                                BEACH SURFER
                                </a>
                            </h5>
                        </div>
                    </div>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <p class="card-text">
                            If you have never surfed before or are still a beginner this course will set you up with some essential knowledge. You’ll learn the art of riding ocean waves with the fundamentals to take your surfing to the next level. The course focuses on basic surfing technique, paddling, popping-up, wave riding stance, turning your board on the wave, ocean knowledge and safety. After taking this course you’ll be surfing beach break waves, feeling comfortable in the ocean and know the correct surfing etiquette.
                        </p>
                        <div class="card mb-3">
                            <div class="card-header">
                                <h6 class="card-title font-weight-bold">Individual Beach Break Course 1-5</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <!-- <p class="font-weight-bold">
                                            Rp 700,000* for 2hrs
                                        </p> -->
                                    </div>
                                    <div class="col">
                                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/5') }}">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-header">
                                <h6 class="card-title font-weight-bold">Packages Course</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <!-- <p class="font-weight-bold">
                                            Rp 3,250,000* for 2hrs x 5
                                        </p> -->
                                    </div>
                                    <div class="col">
                                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/1') }}">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- 
            <div class="card">
                <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <div class="row">
                    <div class="col-5">
                            <img src="{{ asset('img/reef-surfer.jpg') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-7">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed" >
                                LEVEL 2 <br/> 
                                REEF SURFER
                                </a>
                            </h5>
                        </div>
                    </div>
                </div>

                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <p class="card-text">
                            So you can ride waves but want to smooth out those sketchy cut backs and bottom turns? Push yourself to control the extra power of reef break waves and learn how to stay safe. This course teaches you about using natural channels to get out back, reading how a wave breaks, wave take-off positioning and improving manoeuvres like bottom turns, carving and cutbacks. See for yourself when and where you are going wrong of your session, and by the end of this course you’ll know exactly how to keep improving your technique. 
                        </p>
                        <div class="card mb-3">
                            <div class="card-header">
                                <h6 class="card-title font-weight-bold">Individual Beach Break Course 6-10</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <p class="font-weight-bold">
                                            Rp 700,000* for 2hrs
                                        </p> 
                                    </div>
                                    <div class="col">
                                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/6') }}">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col">
                                        <h6 class="card-title font-weight-bold">Packages Course</h6>                                    
                                    </div>  
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                          <p class="font-weight-bold">
                                            Rp 3,250,000* for 2hrs x 5
                                        </p> 
                                    </div>
                                    <div class="col">
                                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/2') }}">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             -->
            <!-- <div class="card">
                <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <div class="row">
                        <div class="col-5">
                            <img src="{{ asset('img/power-surfer.jpg') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-7">
                            <h5 class="mb-0">
                                <a class="btn btn-link collapsed">
                                LEVEL 3 <br/> 
                                POWER SURFER
                                </a>
                            </h5>
                        </div>
                    </div>
                </div>

                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        <p class="card-text">
                            You’re already pushing the limits of your surfing and are now ready to throw yourself in the deep end. This course is designed to help you to step up your game. Learn how to drive off your bottom turn to slash the lip of a wave, master fast rail-to-rail surfing and smoothly link your manoeuvres. Borrowing elements of the Rip Curl Team Rider Manual, this specialized course will make sure you can really nail those manoeuvres and tricks that you’ve been working on. See for yourself when and where you are going wrong with video analysis of your session, and by the end of this course you’ll know exactly how to keep improving your technique.
                        </p>
                        <div class="card mb-3">
                            <div class="card-header">
                                <h6 class="card-title font-weight-bold">Individual Beach Break Course 11-15</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <p class="font-weight-bold">
                                            Rp 700,000* for 2hrs
                                        </p>
                                    </div>
                                    <div class="col">
                                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/7') }}">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-header">
                                <h6 class="card-title font-weight-bold">Packages Course</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <p class="font-weight-bold">
                                            Rp 3,250,000* for 2hrs x 5
                                        </p>
                                    </div>
                                    <div class="col">
                                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/3') }}">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-header">
                                <h6 class="card-title font-weight-bold">Reef Trip</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                         <p class="font-weight-bold">
                                            Rp 1,100,000* for 2hrs
                                        </p> 
                                        <p class="card-text">
                                            * includes : transport. towels, bottled water, sunscreen, equipment, and insurance
                                        </p>
                                    </div>
                                    <div class="col">
                                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/4') }}">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    @elseif($type=='couple')
    <div class="form-group fixed-top">
        <div class="row red">
            <a class="btn ml-3" href="{{ route('home') }}">
                <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
            </a>
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white font-weight-bold">Choose Product</label>
            </div>
        </div>
    </div>
    <div class="form-group content">        
        <p class="card-text text-primary">* All prices are subject to an additional 10% government tax</p>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Private Couple</h6>
                        <!-- <h6>Rp 1,950,000* for 1 hr</h6> -->
                        <p class="card-text">
                            1 instructor for 2 person
                        </p>
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/15') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($type=='family')
    <div class="form-group fixed-top">
        <div class="row red">
            <a class="btn ml-3" href="{{ route('home') }}">
                <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
            </a>
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white font-weight-bold">Choose Product</label>
            </div>
        </div>
    </div>
    <div class="form-group content">
        <p class="card-text text-primary">* All prices are subject to an additional 10% government tax</p>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Family Package</h6>
                        <!-- <h6>Rp 2,950,000* for 1 hr</h6> -->
                        <p class="card-text">
                            2 adults + 2 kids
                        </p>
                    </div>
                    <div class="col">
                        <!-- <button class="mt-c-btn-buy-now btn btn-danger float-right text-white" type="button" data-mt-source="https://megatix.co.id/white-label/family-package">Click to Continue</button> -->
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adult-kid/16') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($type=='private')
    <div class="form-group fixed-top">
        <div class="row red">
            <a class="btn ml-3" href="{{ route('home') }}">
                <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
            </a>
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white font-weight-bold">Choose Product</label>
            </div>
        </div>
    </div>
    <div class="form-group content">
        <p class="card-text text-primary">* All prices are subject to an additional 10% government tax</p>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">VIP - Adult Individual</h6>
                        <!-- <h6>Rp 2,200,000* for 1 hr</h6> -->
                        <p class="card-text">
                            each course, private instructor, private transport and photographer
                        </p>
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/8') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Private</h6>
                         <!-- <h6>Rp 1,100,000* for 1 hr</h6>  -->
                        <p class="card-text">
                            each course, private instructor, sharing transport
                        </p> 
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/9') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Private 3 courses packages</h6>
                        <!-- <h6>Rp 3,000,000* for 1 hr x 3</h6>  -->
                        <p class="card-text">
                            each course, private instructor, sharing transport
                        </p>
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/10') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Private 5 courses packages</h6>
                         <!-- <h6>Rp 4,675,000* for 1 hr x 5</h6>  -->
                        <p class="card-text">
                            each course, private instructor, sharing transport
                        </p>
                    </div>
                    <div class="col">
                        <a role="button" class="btn btn-danger float-right text-white" href="{{ url('/order/adults/11') }}">Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif($type=='rentals') 
    <!-- rentals -->
    <div class="form-group fixed-top">
        <div class="row red">
            <a class="btn ml-3" href="{{ route('home') }}">
                <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
            </a>
            <div class="mx-auto my-auto" style="padding-right:50px;">
                <label class="text-white font-weight-bold">Choose Product</label>
            </div>
        </div>
    </div>
    <div class="form-group content">
        <p class="card-text text-primary">* All prices are subject to an additional 10% government tax</p>
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6 class="card-title font-weight-bold">Rental Full set</h6>
                        <!-- <h6>Rp 550,000* for 1 hr</h6> -->
                        <p class="card-text">
                            Includes rash guards, shorts, impact vest, booties, helmet
                        </p>
                    </div>
                    <div class="col">
                        <button class="mt-c-btn-buy-now btn btn-danger btn-block" id="bayar" type="button" 
                            data-mt-source="https://megatix.com.au/api/v2/accounts/token-redirect?next=/white-label/surf-rental&token={{ Session::get('auth.currentUser')['access_token'] }}">
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection

<style>
    .fixed-top{
        position: fixed;
        top:0;
        width: 100%;
    }
    .content{
        margin-top: 5em;
    }
</style>