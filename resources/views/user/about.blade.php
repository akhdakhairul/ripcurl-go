@extends('layouts.app')
@section('content')
<div class="form-group fixed-top">
    <div class="row red">
        <a class="btn ml-3" href="{{ route('home') }}">
            <i class="fas text-white fa-3x fa-chevron-left fa-lg"></i>
        </a>
        <div class="mx-auto my-auto" style="padding-right:50px;">
            <label class="text-white font-weight-bold">About</label>
        </div>
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <p>
                The Rip Curl School of Surf is Asia’s leading board school. It has an         
                international reputation for offering the finest services, facilities
                and consistently perfect learning conditions. Operating from three
                locations in Bali, the school offers courses at all levels in surfing, wake
                boarding, kite surfing, windsurfing, body boarding and stand-up
                paddle boarding.
                The Rip Curl School of Surf has successfully worked with hotels and
                resorts in Indonesia helping to increasing their room night stays and
                revenues through our supportive relationships with the management
                and our wide range of tailored experiences for their guests. Potential
                guests are drawn to Rip Curl locations due to the reputation of the
                brand and our consistent delivery of world class programs. These
                guests continually return to complete their courses, try new sports
                or simply be part of the good vibes we create. We’ve worked with
                the Prama Hotel in Sanur for 15 years, also with your IHG partner the
                Intercontinental Resort in Jimbaran since 2009, providing unequaled
                service and experiences to their guests.
            </p>
        </div>
    </div>
</div>
@endsection
<style>
    .fixed-top{
        position: fixed;
        top:0;
        width: 100%;
    }
    .content{
        margin-top: 10em;
    }

    .anyclass{
        height:175px;
        overflow-y: scroll;
    }
</style>